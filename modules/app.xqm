xquery version "3.1";

module namespace app = "http://exist-db.org/apps/pprisse/templates";

declare default element namespace "http://www.tei-c.org/ns/1.0";

import module namespace templates = "http://exist-db.org/xquery/html-templating";
import module namespace lib = "http://exist-db.org/xquery/html-templating/lib";
import module namespace config = "http://exist-db.org/apps/pprisse/config" at "config.xqm";

(: Path to the file containing the presentation of the papyrus :)
declare variable $app:presentation := doc($config:data-root || "/xml/2022_pPrisse_Presentation.xml");
(: Path to the file containing the papyrus edition :)
declare variable $app:edition := doc($config:data-root || "/xml/2022_pPrisse_Edition.xml");
(: Path to the XSLT file used for the transformation of the papyrus presentation :)
declare variable $app:presentationXSL := doc($config:app-root || "/resources/xslt/presentation/presentation.xsl");
(: Path to the XSLT file used for the transformation of the papyrus edition :)
declare variable $app:editionXSL := doc($config:app-root || "/resources/xslt/edition/edition.xsl");

(: $app:queryLanguage take the value of the variable "lang" :)
declare variable $app:queryLanguage := request:get-parameter("lang", "");

(: If the variable "lang" is not null, the variable $language is equal to the value of the variable "lang" 
otherwise it is equal to "fr", the application's default language :)
declare function app:getLanguage($queryLanguage as item()) {
    let $language := $queryLanguage
    return
        if ($language)
        then
            $language
        else
            "fr"
};

(: Set the variable $app:lang as the value :)
declare variable $app:lang := app:getLanguage($app:queryLanguage);
(: Set the variable $app:lang as parameters for the transform:transform function :)
declare variable $app:langAsParameter := <parameters><param name="lang" value="{$app:lang}"/></parameters>;

(: This function selects title based on language defined by the variable $app:lang. 
The title contains an <a> element with an empty @href attribute to reload the page:)
declare function app:title($node as node(), $model as map(*)) {
    let $title := $app:presentation/descendant::div[@type="presentation" and @xml:lang=$app:lang]/child::div[1]/head
    return
        <h1 data-i18n="appTitle">
            <a href="">{$title/string()}</a>
        </h1>
};

(: This function defines the content of the index.html page 
indicating the need for screen rotation according to the language $app:lang :)
declare function app:rotateScreen($node as node(), $model as map(*)) {
    switch ($app:lang)
    case "en" return <div data-i18n="rotateScreen">Please change the orientation of your screen</div>
    case "ar" return <div data-i18n="rotateScreen">يرجى تغيير اتجاه الشاشة</div>
    default return <div data-i18n="rotateScreen">Merci de changer l'orientation de votre écran</div>
};

(: This function creates the list of selectable languages based on the languages used and described 
in the XML edition of the papyrus excluding hieratic Egyptian (egy-Egyh) from the result 
and according to the language $app:lang :)
declare function app:languageButtons($node as node(), $model as map(*)) {
    <ul class="dropdownContent">{
        for $language in $app:edition/descendant::language[@ident!="egy-Egyh"]
        let $name := $language/child::lang[@xml:lang=$app:lang]/string()
        let $value := $language/@ident
        return
            <li class="dropdownItem">
                <input type="hidden" value="{$value}" name="lang"/>
                <button type="submit" class="changeLang">
                    <img src="{concat("resources/images/icons_language/flag_",$value,".png")}"/>
                    <span data-i18n="{concat("languageButtons_", $value)}">{$name}</span>
                </button>
            </li>
    }</ul>
};

(: This function generates right sidebar content, mainly checkboxes, according to the language $app:lang :)
declare function app:rightSidebar($node as node(), $model as map(*)) {
    switch ($app:lang)
    case "en" return
        <div class="sidebarContent">
            <h2 data-i18n="rightSidebarTitle">Show/Hide:</h2>
            <ul>
                <li>
                    <input type="checkbox" id="checkbox_jointright" class="checkbox" autocomplete="off" value="zone_jointright"/>
                    <label class="checkbox_label" for="checkbox_jointright" data-i18n="checkbox_jointright">The papyrus right-hand junctions</label>
                </li>
                <li>
                    <input type="checkbox" id="checkbox_jointleft" class="checkbox" autocomplete="off" value="zone_jointleft"/>
                    <label class="checkbox_label" for="checkbox_jointleft" data-i18n="checkbox_jointleft">The papyrus left-hand junctions</label>
                </li>
                <li>
                    <input type="checkbox" id="checkbox_polyptoton" class="checkbox" autocomplete="off" value="zone_polyptoton"/>
                    <label class="checkbox_label" for="checkbox_polyptoton" data-i18n="checkbox_polyptoton">The occurences of the word <i>sedjem</i></label>
                </li>
                <li>
                    <input type="checkbox" id="checkbox_rubric" class="checkbox" autocomplete="off" value="zone_rubric"/>
                    <label class="checkbox_label" for="checkbox_rubric" data-i18n="checkbox_rubric">The areas written in red ink</label>
                </li>
                <!--<li>
                    <input type="checkbox" id="checkbox_erasedText" class="checkbox" autocomplete="off" value="erasedText" />
                    <label class="checkbox_label" for="checkbox_erasedText" data-i18n="checkbox_erasedText">The traces of the erased text</label>
                </li>-->
            </ul>
        </div>
    case "ar" return
        <div class="sidebarContent">
            <h2 data-i18n="rightSidebarTitle">عَرْض/إخفاء:</h2>
            <ul>
                <li>
                    <input type="checkbox" id="checkbox_jointright" class="checkbox" autocomplete="off" value="zone_jointright"/>
                    <label class="checkbox_label" for="checkbox_jointright" data-i18n="checkbox_jointright">الحشوات اليمنى للبردية</label>
                </li>
                <li>
                    <input type="checkbox" id="checkbox_jointleft" class="checkbox" autocomplete="off" value="zone_jointleft"/>
                    <label class="checkbox_label" for="checkbox_jointleft" data-i18n="checkbox_jointleft">الحشوات اليسرى للبردية</label>
                </li>
                <li>
                    <input type="checkbox" id="checkbox_polyptoton" class="checkbox" autocomplete="off" value="zone_polyptoton"/>
                    <label class="checkbox_label" for="checkbox_polyptoton" data-i18n="checkbox_polyptoton">تكرار كلمة <i>سدﭽم</i></label>
                </li>
                <li>
                    <input type="checkbox" id="checkbox_rubric" class="checkbox" autocomplete="off" value="zone_rubric"/>
                    <label class="checkbox_label" for="checkbox_rubric" data-i18n="checkbox_rubric">المناطق المكتوبة بالحبر الأحمر</label>
                </li>
                <!--<li>
                    <input type="checkbox" id="checkbox_erasedText" class="checkbox" autocomplete="off" value="erasedText"/>
                    <label class="checkbox_label" for="checkbox_erasedText" data-i18n="checkbox_erasedText"></label>
                </li>-->
            </ul>
        </div>
    default return
        <div class="sidebarContent">
            <h2 data-i18n="rightSidebarTitle">Afficher/Masquer :</h2>
            <ul>
                <li>
                    <input type="checkbox" id="checkbox_jointright" class="checkbox" autocomplete="off" value="zone_jointright"/>
                    <label class="checkbox_label" for="checkbox_jointright" data-i18n="checkbox_jointright">Les joints droits du papyrus</label>
                </li>
                <li>
                    <input type="checkbox" id="checkbox_jointleft" class="checkbox" autocomplete="off" value="zone_jointleft"/>
                    <label class="checkbox_label" for="checkbox_jointleft" data-i18n="checkbox_jointleft">Les joints gauches du papyrus</label>
                </li>
                <li>
                    <input type="checkbox" id="checkbox_polyptoton" class="checkbox" autocomplete="off" value="zone_polyptoton"/>
                    <label class="checkbox_label" for="checkbox_polyptoton" data-i18n="checkbox_polyptoton">Les occurences du mot <i>sedjem</i></label>
                </li>
                <li>
                    <input type="checkbox" id="checkbox_rubric" class="checkbox" autocomplete="off" value="zone_rubric"/>
                    <label class="checkbox_label" for="checkbox_rubric" data-i18n="checkbox_rubric">Les zones rédigées à l'encre rouge</label>
                </li>
                <!--<li>
                    <input type="checkbox" id="checkbox_erasedText" class="checkbox" autocomplete="off" value="erasedText"/>
                    <label class="checkbox_label" for="checkbox_erasedText" data-i18n="checkbox_erasedText">Les traces du texte effacé</label>
                </li>-->
            </ul>
        </div>
};

(: This function generates the buttons to display the translation of the papyrus 
according to the language $app:lang :)
declare function app:translationButton($node as node(), $model as map(*)) {
    switch ($app:lang)
    case "en" return
        <button class="translationButton">
            <span class="showTranslationButton" data-i18n="showTranslationButton">Read the papyrus</span>
            <span class="hideTranslationButton" data-i18n="hideTranslationButton">Stop reading the papyrus</span>
        </button>
    case "ar" return
        <button class="translationButton">
            <span class="showTranslationButton" data-i18n="showTranslationButton">قراءة البردية</span>
            <span class="hideTranslationButton" data-i18n="hideTranslationButton">التوقف عن قراءة البردية</span>
        </button>
    default return
        <button class="translationButton">
            <span class="showTranslationButton" data-i18n="showTranslationButton">Lire le papyrus</span>
            <span class="hideTranslationButton" data-i18n="hideTranslationButton">Cesser la lecture du papyrus</span>
        </button>
};

(: This function generates the button to display the transliteration of the papyrus 
according to the language $app:lang :)
declare function app:transliterationButton($node as node(), $model as map(*)) {
    switch ($app:lang)
    case "en" return
        <button class="toggleTransliteration">
            <span class="showTransliterationButton" data-i18n="showTransliterationButton">Show the transliteration</span>
            <span class="hideTransliterationButton" data-i18n="hideTransliterationButton">Hide the transliteration</span>
        </button>
    case "ar" return
        <button class="toggleTransliteration">
            <span class="showTransliterationButton" data-i18n="showTransliterationButton">عَرْض الكتابة الصوتية</span>
            <span class="hideTransliterationButton" data-i18n="hideTransliterationButton">إخفاء الكتابة الصوتية</span>
        </button>
    default return
        <button class="toggleTransliteration">
            <span class="showTransliterationButton" data-i18n="showTransliterationButton">Afficher la translittération</span>
            <span class="hideTransliterationButton" data-i18n="hideTransliterationButton">Masquer la translittération</span>
        </button>
};


(: This function generates the buttons to display the translation of the papyrus 
according to the language $app:lang :)
declare function app:hieroglyphButton($node as node(), $model as map(*)) {
    switch ($app:lang)
    case "en" return
        <button class="toggleHieroglyph">
            <span class="showHieroglyphButton" data-i18n="showHieroglyphButton">Show hieroglyphic transcription</span>
            <span class="hideHieroglyphButton" data-i18n="hideHieroglyphButton">Hide hieroglyphic transcription</span>
        </button>
    case "ar" return
        <button class="toggleHieroglyph">
            <span class="showHieroglyphButton" data-i18n="showHieroglyphButton">عَرْض النَّسْخ الهيروغليفي</span>
            <span class="hideHieroglyphButton" data-i18n="hideHieroglyphButton">إخفاء النَّسْخ الهيروغليفي</span>
        </button>
    default return
        <button class="toggleHieroglyph">
            <span class="showHieroglyphButton" data-i18n="showHieroglyphButton">Afficher la transcription hiéroglyphique</span>
            <span class="hideHieroglyphButton" data-i18n="hideHieroglyphButton">Masquer la transcription hiéroglyphique</span>
        </button>
};

(: This function generates the buttons allowing to display the different parts of the presentation 
by selecting the associated illustration image, this one bearing a name containing the value of the position of this part in the XML document, 
and the title of this part according to the language $app:lang :)
declare function app:presentationButtons($node as node(), $model as map(*)) {
    let $parts := $app:presentation/descendant::div[@type="presentation" and @xml:lang=$app:lang]/descendant::div[@type="part"]
    for $part at $pos in $parts[@n!='introduction']
    let $partTitle := $part/child::head
    return
        <button class="presentationButtons {$part/@n}">
            <img class="presentationButtonsImg" src="{concat("$myresources/images/icons_sidebar/icon_heading_", $pos, ".png")}" alt="icons"/>
            <span data-i18n="{concat("button_", $pos)}">{$partTitle/string()}</span>
        </button>
};

(: This function transforms the selected div[@type="presentation"] element according to the language $app:lang and passes the value of the variable $app:langAsParameter as a parameter.
This parameter is used for copyright generation in the pprisse/resources/xslt/presentation/div.xsl file :)
declare function app:presentation($node as node(), $model as map(*)) {
    <div class="presentationContainer">{
        let $part := $app:presentation/descendant::div[@type="presentation" and @xml:lang=$app:lang]
        return
            transform:transform($part, $app:presentationXSL, $app:langAsParameter)
    }</div>
};

(: This function transforms the div[@type="edition"] element and passes the value of the variable $app:langAsParameter as a parameter.
This parameter is used in the pprisse/resources/xslt/edition/body.xsl file to select the language of the translation to display :)
declare function app:translation($node as node(), $model as map(*)) {
        let $text := $app:edition/descendant::body
        return
            transform:transform($text, $app:editionXSL, $app:langAsParameter)
};

(: The variable $app:currentVerse corresponds to the currently displayed verse
passed by an Ajax request described in app.js, function summary(verse) :)
declare variable $app:currentVerse := request:get-parameter("currentVerse", "");

(: This function generates a list of options listing all the texts present on the papyrus 
by adding the @selected attribute with the value "selected" to the option corresponding to the text containing the currently displayed verse :)
declare function app:getText($verse as xs:string) {
    <select id="selectText">{
        for $text at $pos in $app:edition/descendant::div[@type="translation" and @xml:lang=$app:lang]/descendant::div[@type="text"]
        let $head := transform:transform($text/child::head, $app:editionXSL, ())
        let $firstVerse := substring-after($text/descendant::l[1]/@corresp, "#")
        let $id := concat("text_", $pos)
        order by $pos
        return
            if ($text[descendant::l[@corresp=concat("#", $verse)]])
            then
                <option selected="selected" value="{$firstVerse}" data-i18n="{$id}">{$head}</option>
            else
                <option value="{$firstVerse}" data-i18n="{$id}">{$head}</option>
    }</select>
};

(: This function generates a list of options listing all the parts of the text containing the currently displayed verse 
and adds the @selected attribute with the value "selected" to the option corresponding to the part containing the verse :)
declare function app:getTextPart($verse as xs:string) {
    let $text := $app:edition/descendant::div[@type="translation" and @xml:lang=$app:lang]/descendant::div[@type="text" and descendant::l[@corresp=concat("#", $verse)]]
    return
        <select id="selectTextPart">{
            for $part at $pos in $text/descendant::div[@type="part"]
            let $head := transform:transform($part/child::head, $app:editionXSL, ())
            let $firstVerse := substring-after($part/descendant::l[1]/@corresp, "#")
            let $id := concat("part_", $pos)
            order by $pos
            return
                if ($part[descendant::l[@corresp=concat("#", $verse)]])
                then
                    <option selected="selected" value="{$firstVerse}" data-i18n="{$id}">{$head}</option>
                else
                    <option value="{$firstVerse}" data-i18n="{$id}">{$head}</option>
        }</select>
};

(: This function generates an option list listing all the subparts of the part containing the verse currently displayed 
and adds the attribute @selected with the value "selected" to the option corresponding to the subpart containing the verse :)
declare function app:getTextSubPart($verse as xs:string) {
    let $part := $app:edition/descendant::div[@type="translation" and @xml:lang=$app:lang]/descendant::div[@type="part" and descendant::l[@corresp=concat("#", $verse)]]
    return
        <select id="selectTextSubPart">{
            for $subpart at $pos in $part/descendant::div[@type="subpart"]
            let $head := transform:transform($subpart/child::head, $app:editionXSL, ())
            let $firstVerse := substring-after($subpart/descendant::l[1]/@corresp, "#")
            let $id := concat("subpart_", $pos)
            order by $pos
            return
                if ($subpart[descendant::l[@corresp=concat("#", $verse)]])
                then
                    <option selected="selected" value="{$firstVerse}" data-i18n="{$id}">{$head}</option>
                else
                    <option value="{$firstVerse}" data-i18n="{$id}">{$head}</option>
        }</select>
};

(: This function selects the <l> element corresponding to the currently displayed verse 
and displays the result of the three previous functions according to the type of the parent of this element :)
declare function app:summary($node as node(), $model as map(*)) {
    let $currentVerse := $app:currentVerse
    let $currentl := $app:edition/descendant::l[@corresp=concat("#", $currentVerse)]
    return
        if ($currentl[ancestor::div[@type="subpart"]])
        then
            (app:getText($currentVerse), app:getTextPart($currentVerse), app:getTextSubPart($currentVerse))
        else
            if ($currentl[ancestor::div[@type="part"]])
            then
                (app:getText($currentVerse), app:getTextPart($currentVerse))
            else
                app:getText($currentVerse)
};

(: This function generates the display of the verse being read $app:currentVerse
and changes the caption associated with the verse number according to the language $app:lang :)
declare function app:location($node as node(), $model as map(*)) {
    let $currentVerse := $app:currentVerse
    let $location := <span class="currentVerse">{
                        switch ($app:lang)
                        case "ar" return translate($currentVerse, "1234567890GPb", "١٢٣٤٥٦٧٨٩٠جٮٮ")
                        default return $currentVerse
                     }</span>
    let $locationLegend := switch ($app:lang)
                           case "en" return <span class="locationLegend" data-i18n="locationLegend">Verse: </span>
                           case "ar" return <span class="locationLegend" data-i18n="locationLegend">مقولة: </span>
                           default return <span class="locationLegend" data-i18n="locationLegend">Vers : </span>
    return
        ($locationLegend, $location)
};




declare function app:getVerse($node as node(), $model as map(*)) {
    let $requestedVerses := request:get-parameter("verses", "")
    let $verses := if (contains($requestedVerses, "_"))
                   then
                    let $vs := tokenize($requestedVerses, "_")
                    return
                        for $v in $vs
                        return
                            $app:edition/descendant::div[@type="hieroglyphicEncoding"]/descendant::l[@xml:id=$v]/@xml:id/string()
                   else
                    let $v1 := tokenize($requestedVerses, "\|")[1]
                    let $vX := tokenize($requestedVerses, "\|")[last()]
                    let $first := count($app:edition/descendant::div[@type="hieroglyphicEncoding"]/descendant::l[@xml:id=$v1]/preceding::l)+1
                    let $last := count($app:edition/descendant::div[@type="hieroglyphicEncoding"]/descendant::l[@xml:id=$vX]/preceding::l)+1
                    return
                        for $i in $first to $last
                        return
                            $app:edition/descendant::div[@type="hieroglyphicEncoding"]/descendant::l[$i]/@xml:id/string()
    let $requestedTypes := request:get-parameter("type", "")
    let $requestedLang := request:get-parameter("lang", "")
    return
        for $val in $verses
        let $v := <div class="resultId">{$val}</div>
        let $hiero := if (contains($requestedTypes, "h"))
                      then
                        <div class="resultImage">{
                            <img src="{concat('$mydata/hieroglyphs/',$val,'.svg')}"/>
                        }</div>
                      else
                        ()
        let $mdc := if (contains($requestedTypes, "mdc"))
                    then
                        <div class="resultmdc">{
                        $app:edition/descendant::div[@type="hieroglyphicEncoding"]/descendant::l[@xml:id=$val]
                        }</div>
                    else
                        ()
        let $edition  := if (contains($requestedTypes, "e"))
                         then
                            <div class="resultEdition">{
                                transform:transform($app:edition/descendant::div[@type="edition"]/descendant::l[@corresp=concat("#", $val)], $app:editionXSL, ())
                            }</div>
                         else
                             ()
        let $translation := if ($requestedLang!="")
                            then
                                for $langs in tokenize($requestedLang, "\|")
                                return
                                    <div class="resultTranslation">{
                                        transform:transform($app:edition/descendant::div[@type="translation" and @xml:lang=$langs]/descendant::l[@corresp=concat("#", $val)], $app:editionXSL, ())
                                    }</div>
                            else
                                ()
        return
            if ($requestedVerses != "")
            then
                <div class="resultVerse">{
                    $v, $hiero, $mdc, $edition, $translation
                }</div>
            else
                ()
};