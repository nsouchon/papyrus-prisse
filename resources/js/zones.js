/*
 * Code for manipulating zones on the papyrus.
 * 
 * The code you should use is :
 * - the ZonesManager class, which is your main access point to zones
 * - the Zone class for individual zones ;
 * - the BoundingBox class
 * 
 * To create and use the zones manager, write :
 * 
 * let zonesManager = new ZonesManager()
 * 
 * Then you can call its methods.
 * 
 * 
 */


/**
 * Main class 
 */
 class ZonesManager {

    constructor() {
        this.zones = _prepareZones();
        this.verseZones = this.zones.filter(z => z.getZoneType() === "verse")
    }

    /**
     * Returns all zones
     * @returns {Zone[]}
     */
    getZones() {
        return this.zones;
    }

    /**
     * Returns all verse zones.
     * @returns {Zone[]}
     */
    getVerseZones() {
        return this.verseZones;
    }

    /**
     * Returns all zones for a given verse.
     * @param {String} verseLabel 
     * @returns {Zone[]}
     */
    getZonesForAVerse(verseLabel) {
        return this.verseZones.filter(z => 
            z.getData().verse == verseLabel
        );
    }

    /**
     * Add all zones to a given viewer.
     * @param {*} viewer 
     */
    addAllZonesToViewer(viewer) {
        let overlay = viewer.svgOverlay();

        for (let zone of this.getZones()) {
            //if (zone.getZoneType() === "verse") {
            //    overlay.onClick(zone.svg, (e) => alert("vers "+ zone.data.verse))
            //}
            overlay.node().appendChild(zone.getSvg())
        }
    }
}

/**
 * Definition of a zone.
 */
class Zone {

    constructor(zoneType, points, data, idx) {
        this.zoneType = zoneType;
        this.points = points;
        this.data = data;
        this.idx = idx;
        this.bbox = _computeBoundingBox(points);
        this.svg = _createSVGForZone(zoneType, points)
        if (zoneType === "verse") {
            this.svg.classList.add("verse_" + data.verse)
            this.svg.id = idx
        } else {
            this.svg.id = idx
        }
    }

    /**
     * Returns the type of zone ("verse", "rubric", etc...)
     * @returns {String} 
     */
    getZoneType() {
        return this.zoneType;
    }

    /**
     * Returns the list of points in this zone.
     * @returns {Array} a list of points having x,y coordinates.
     */
    getPoints() {
        return this.points;
    }

    /**
     * Returns a bounding box for this zone.
     * It can be used to zoom to the zone, as it's a plain rectangle including the whole zone.
     * 
     * The class also has method to merge bounding boxes when one wants to display multiple zones at once.
     * @returns {BoundingBox}
     */
    getBoundingBox() {
        return this.bbox;
    }

    /**
     * @returns {Object} zone-specific data. Most notably, for verse, will have fields "verse" and "indexInVerse".
     */
    getData() {
        return this.data;
    }
    
    /**
     * Returns an SVG DOM element, which can be added to the viewer if needed.
     * It will have the following CSS classes : 
     * - annotation
     * - zoneVerse, zoneRubric, etc... to indicate its type ;
     * - each verse is associated with a class, called "verse_" + label of the verse (e.g. verse_P1)
     * 
     * Note that the element is an SVG group ; it might be interesting later to be able to style
     * its parts individually (perhaps through specific CSS classes)
     * @returns {Element} 
     */
    getSvg() {
        return this.svg;
    }


    /**
     * Adds a onclick function for a zone.
     * (this method might need further work)
     * @param {*} viewer the OpenSeadragon viewer.
     * @param {*} onclickFunction a function which takes a Zone (this class) and a DOM element as argument.
     */
    onClick(viewer, onclickFunction) {
        let thisZone = this;
        let overlay = viewer.svgOverlay();
        overlay.onClick(this.getSvg(), function (elt) {
            onclickFunction(thisZone, elt);
        });
    }
}


class BoundingBox {

    constructor(xmin, ymin, width, height) {
        this.xmin = xmin;
        this.ymin = ymin;
        this.width = width;
        this.height = height;
    }

    getXMin() {
        return this.xmin;
    }

    getYMin() {
        return this.ymin;
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }
    
    getXMax() {
        return this.getXMin() + this.getWidth()
    }

    getYMax() {
        return this.getYMin() + this.getHeight()
    }

    /**
     * Create a Bounding box which encompasses this bbox and the argument.
     * @param {BoundingBox} otherBBox 
     * @returns {BoundingBox}
     */
    merge(otherBBox) {
        let xmin = Math.min(this.getXMin(), otherBBox.getXMin());
        let ymin = Math.min(this.getYMin(), otherBBox.getYMin());
        let xmax = Math.max(this.getXMax(), otherBBox.getXMax());
        let ymax = Math.max(this.getYMax(), otherBBox.getYMax());
        let width = xmax - xmin;
        let height = ymax - ymin;
        return new BoundingBox(xmin, ymin, width, height);        
    }
}



// Auxiliary function to compute bbox from points.
function _computeBoundingBox(points) {
    if (points.length === 0) {
        // Normally not defined, but avoid problems :
        return new BoundingBox(0,0,0,0);
    } else {
        let p0 = points[0]
        let xmin = p0.x
        let ymin = p0.y
        let xmax = p0.x
        let ymax = p0.y
        for (let p of points) {
            xmin = Math.min(xmin, p.x)
            ymin = Math.min(ymin, p.y)
            xmax = Math.max(xmax, p.x)
            ymax = Math.max(ymax, p.y)
        }
        return new BoundingBox(xmin, ymin, xmax - xmin, ymax - ymin);
    }
}

/** 
 * Returns a document-wide list of zones.
 * 
 * Each processed zone will have the following fields :
 * 
 * - zoneType
 * - points (in global papyrus coordinates)
 * - data : zone-specific information
 * - svg : an SVG DOM element, which can be added to the viewer if needed.
 *         it will have the following classes : annotation and zoneVerse, zoneRubric (or any type the zone has).
 * 
 * Actual work will probably :
 * a) filter zones according to their values, delete or recreate them
 * b) change their css characteristics (adding/removing classes would be 
 *    a solution)
 * 
 * This function uses a number of global variables. This should be modified at some point to make
 * dependencies explicit.
 * 
 * We currently use the following variables :
 * viewer : created by "viewer.js", an OpenSeaDragon viewer object
 * zonesJSON : created by zoneDefinitions.js (all zones, a dump from the editor software)
 * platesGeometry : defined in PlatesGeometry.js : knows the exact position of each plate
 * multiPlateGeometry : created by "viewer.js" : conversion system from coordinates in one plate to global papyrus coordinates.
 */

function _prepareZones() {
    // Note : multiPlateGeometry should be modified
    // to be able to use plate name instead of index
    // Also note : indexes are reversed in editor and viewer
    let platesToIndex = {}
    for (let i = 0; i < platesGeometry.length; i++) {
        let plate = platesGeometry[i]
        platesToIndex[plate.dzi] = i
    }
    let zones = []
    for (let plate of zonesJSON) {
        let plateIndex = platesToIndex[plate.dzi]
        for (let zOrig of plate.zones) {     
            let points = []      
            for (let p of zOrig.points) {
                let newP =
                    multiPlateGeometry.fromPlateToMultiplate(
                        plateIndex, p.x, p.y)
                points.push(newP)
            }
            let newZone = new Zone(zOrig.zoneType, points, zOrig.data, zOrig.idx)
            zones.push(newZone)
        }     
    }
    return zones   
}

// (already defined in viewer - where most of this code should move...)
// const svgNameSpace = 'http://www.w3.org/2000/svg';

function _pointsToSVGString(points) {
    let tab = []
    for (let p of points) {
        tab.push(p.x + "," + p.y)
    }
    return tab.join(" ")
}

/**
 * Creates an SVG rectangle to display a given zone
 * @param {*} zoneType
 * @param {*} points 
 * @returns 
 */

function _createSVGForZone(zoneType, points) {
    let g = document.createElementNS(svgNameSpace, "g")
    let outline = document.createElementNS(svgNameSpace, "polygon")
    outline.setAttributeNS(null, "points", _pointsToSVGString(points))
    g.appendChild(outline)
    g.classList.add("annotation")
    g.classList.add("zone_"+zoneType)
    return g;
}

/**
 * Zone manager comme variable globale pour l'instant.
 */
let zonesManager = new ZonesManager();
zonesManager.addAllZonesToViewer(viewer);