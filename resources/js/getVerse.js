var loader = $(".loader");

/* LOADER */
 /* Display the loader when the page is loading. */
$(window).on("load", function () {
    loader.fadeOut("slow");
});

/* Set the buttons to open and close the tutorial modal */
$(".helpButton, .closeModal").on("click", function () {
    $(".modal").toggle();
});

/* Close the modal if the user clicks outside its content */
$(document).on("click", function (e) {
    if (!(($(e.target).closest(".modalContent").length > 0) || ($(e.target).closest(".helpButton").length > 0))) {
        $(".modal").hide();
    }
});

/* Open the tutorial modal on the first visit */
$(function () {
   var visit = localStorage.getItem("visit");
   if (visit == null) {
       localStorage.setItem("visit", 1);
       $(".modal").show();
   }
});