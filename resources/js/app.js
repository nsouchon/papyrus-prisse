/* GENERAL VARIABLE DECLARATION */
var html = $("html");
var loader = $(".loader");
var orientation = $(".orientation");
var language = $(".languageButton");
var dropdownLanguage = $(".dropdownContent");
var sidebarIcons = $(".sidebarIcons");
var sidebarRight = $(".sidebarRight");
var sidebarLeft = $(".sidebarLeft");
var presentation = $("#presentation");
var presentationPart = $(".presentationPart");
var translation = $("#translation");
var selectText = $('#selectText');
var selectTextPart = $('#selectTextPart');
var selectTextSubPart = $('#selectTextSubPart');
var audio = document.getElementById('audio');
var zonesVerse = $(".zone_verse");

/* PROGRESSIVE WEB APP */
 /* PWA Configuration. Register Service Worker. */
if ('serviceWorker' in navigator) {
    window.addEventListener('load', function () {
        navigator.serviceWorker.register("./service-worker.js");
    });
};

/* LANGUAGE SELECTION */
 /* This function displays the language selection list.
 * When clicking on the page, if the closest element is the .languageButton element, the list is displayed, otherwise it is hidden.
 * In this way, this function also makes it possible to hide the list when this one is displayed and that one clicks out of the list. */
$(document).click(function (event) {
    if ($(event.target).closest(language).length) {
        dropdownLanguage.show();
    } else {
        dropdownLanguage.hide();
    };
});

/* This function loads the CSS properties required for the Arabic version. */
function arabicCSS () {
    $("h1, .changeLang, h2, label, option, span, p, .caption, td, li, .translation, header, .changeLang, .swiper-slide, .translationMenu, .summary, .location, .sidebarRight li, .acknowledgements h2, .acknowledgements ul, body, .sidebarLeft, .sidebarRight, #presentation, #presentation.show").addClass("ar");
    $("head").append("<link id=\"introJSrtl\" rel=\"stylesheet\" type=\"text/css\" href=\"resources/libs/intro-6.0.0/css/introjs-rtl.css\"/>");
};

/* This function removes the CSS properties added for the Arabic version. */
function removeArabicCSS () {
    $("h1, .changeLang, h2, label, option, span, p, .caption, td, li, .translation, header, .changeLang, .swiper-slide, .translationMenu, .summary, .location, .sidebarRight li, .acknowledgements h2, .acknowledgements ul, body, .sidebarLeft, .sidebarRight, #presentation, #presentation.show").removeClass("ar");
    $('head').find('link#introJSrtl').remove();
};

/* SWITCH LANGUAGE */
 /* The next two functions allow you to change the language of the document.
 * When clicking on one of the .changeLang buttons, an Ajax request is sent to retrieve the index.html page with the lang parameter whose value is equal to that of the clicked button.
 * The returned data is then parsed to retrieve elements bearing the data-i18n attribute and use them to replace those currently displayed. */
function switchLanguage(language, flag) {
    $.ajax({
        url: "index.html",
        method: "get",
        data: {
            lang: language
        },
        /* The data retrieved is of html type */
        dataType: "html",
        ajaxSend: function () {
            /* Displays the loader when the Ajax request is processed. */
            loader.show();
        },
        complete : function () {
            /* Once the Ajax request completes, the loader fades out. */
            loader.fadeOut("slow");
        },
        success: function (data) {
            /* If the request is successful, we parse the result */
            var parsedHTML = $.parseHTML(data);
            /* For each element carrying the data-i18n attribute, */
            $("[data-i18n]").each(function () {
                /* we retrieve the value of this attribute, */
                var i18nAttr = $(this).attr("data-i18n");
                /* we search the parsed result of the request for the element bearing a data-i18n attribute of the same value  */
                var newElement = $(parsedHTML).filter("*").find('[data-i18n="' + i18nAttr + '"]');
                /* and we replace the first with the second. */
                $(this).replaceWith(newElement);
            });
            /* We change the value of the @lang attribute of the <html> element to that of the value variable. */
            html.attr("lang", language);
            /* We change the language of the introduction using the chooseIntroLanguage() function defined in intro.js */
            chooseIntroLanguage();
            /* We change the language icon by replacing the value of the @src attribute of the .languageIcon element child of the .languageButton button with that of the imgURL variable. */
            $(".languageIcon").attr("src", flag);
            /* We display or hide the audio button */
            displayAudioButton();
            /* If the translation of the text is visible, we use the summary() function with the value of the verse number currently displayed as an argument. */
            if (translation.is(":visible")) {
                /* The currently displayed verse is the .swiper-slide-active element. */
                var activeSlide = $(".swiper-slide-active")
                /* The verse_ + verseNumber class of the currently displayed verse is obtained using the getVerse() function with the value of the activeSlide variable as parameter. */
                var verse = getVerse(activeSlide);
                /* The currently displayed verse number is equal to the segment of the string following the underscore character. */
                var verseNumber = verse.substr(verse.indexOf("_") + 1);
                /* We reload the summary using the summary() function with the currently displayed verse number as argument. */
                summary(verseNumber);
                toggletranslationButtonText();
                toggleTransliterationButtonText();
                toggleHieroglyphButtonText();
            };
            /* If the language value is "ar", we apply the arabicCSS function otherwise, we apply the removeArabicCSS function. */
            if (language === "ar") {
                arabicCSS();
            } else {
                removeArabicCSS();
            };
        }
    });
};

$(function () {
    $(".changeLang").click(function (e) {
        e.preventDefault();
        /* The value variable is equal to that of the <input> element preceding the selected button. */
        var value = $(this).prev("input").val();
        /* The imgURL variable is equal to that of the @src attribute of the child <img> element of the selected button.*/
        var imgURL = $(this).children("img").attr("src")
        /* We store the selected language value */
        localStorage.setItem("lang", value);
        /* We call the switchLanguage function */
        switchLanguage(value, imgURL);
    });
});

/* When the page loads, we check the stored language value and apply the switchLanguage function. */
$(function () {
    /* We retrieve the stored language value. */
    var lang = localStorage.getItem("lang");
    /* We define the flag corresponding to the language */
    var flag = "resources/images/icons_language/flag_"+lang+".png";
    /* If a language value is stored, we apply the switchLanguage function. */
    if (lang) {
        switchLanguage(lang, flag);
    }
});

/* LOADER */
 /* Display the loader when the page is loading. */
$(window).on("load", function () {
    loader.fadeOut("slow");
});

/* SIDEBARS */
 /* This function allows you to toggle the .clicked class to the sidebar and thus open or close it. */
function openCloseSidebar(sidebar) {
    sidebar.toggleClass("clicked");
};

/* LEFT SIDEBAR */
 /* This function allows to open or close the left sidebar by clicking on the .sidebarLeftButton button. */
$(function () {
    $(".sidebarLeftButton").click(function () {
        /* Toggle class .clicked. */
        openCloseSidebar(sidebarLeft);
        /* Hide the translation if visible. */
        if (translation.is(":visible")) {
            toggleTranslation();
        };
        /* Stop the audio if playing. */
        if (! audio.paused) {
            playPauseAudio();
        }
    });
});

/* RIGHT SIDEBAR */
 /* This function allows to open or close the right sidebar by clicking on the .sidebarRightButton button. */
$(function () {
    $(".sidebarRightButton").click(function () {
        /* Toggle class .clicked. */
        openCloseSidebar(sidebarRight);
    });
});

/* RIGHT SIDEBAR CHECKBOX */
 /* This function is used to toggle the zones having a class equal to the value of the selected checkbox
 * and to change the color of the label of this checkbox according to the color of the zone. */
$(function () {
    $(".checkbox").change(function () {
        /* The value variable has the value of the selected checkbox. */
        var value = $(this).val();
        /* Toggle the zones having for class the value of the box. */
        $(".annotation." + value).toggle();
        /* Toggle class .activeCheckbox
         *  and change the color of the checkbox to that of the associated zones. */
        $(this).toggleClass("activeCheckbox");
    });
});

/* PRESENTATION */
 /* This function is used to display the presentation. */
function togglePresentation() {
    /* Close the right sidebar if visible. */
    if (sidebarRight.hasClass("clicked")) {
        openCloseSidebar(sidebarRight);
    };
    /* If the presentation is not already displayed, */
    if (! presentation.hasClass("show")) {
        /* we hide all left sidebar icons */
        sidebarIcons.css("visibility", "hidden");
        /* and we toggle the class .show. */
        presentation.toggleClass("show");
    };
};

/* This function is used to display the part of the presentation corresponding to the selected button. */
function showPresentationPart(part) {
    /* Get a list of part element classes. */
    var classList = part.attr('class').split(/\s/);
    /* Stops the video of part six if it is played. */
    if ($("#ytIframe").is(":visible")) {
        stopAndRestartVideo();
    };
    /* Hide all presentation parts */
    presentationPart.hide();
    /* For each value of the classList variable */
    $.each(classList, function (index, item) {
        /* and for each part of the presentation,  */
        $(presentationPart).each(function () {
            /* if it shares the same class, */
            if ($(this).hasClass(item)) {
                /* display the part */
                $(this).show();
            };
        });
    });
};

/* This function adds the .selected class to the selected button
 * and displays the associated part of the presentation.
 * The .selected class is removed from all other buttons. */
$(function () {
    $(".presentationButtons").click(function () {
        /* We remove the .selected class from all .presentationButtons. */
        $(".selected").removeClass("selected");
        /* We add the .selected class to the selected .presentationButtons button. */
        $(this).addClass("selected");
        /* We display the presentation if it is not already displayed. */
        togglePresentation();
        /* We display the part of the presentation associated with the selected button. */
        showPresentationPart($(this));
    });
});

/* This function makes it possible to display one or the other of the figures of part nine by clicking on it. */
$(function () {
    $(".partNine figure").click(function () {
        $(".partNine figure").each(function () {
            if ($(this).is(":visible")) {
                /* If the figure is displayed, we hide it */
                $(this).hide();
            } else {
                /* Otherwise we change the value of its css display property from none to flex */
                $(this).css("display", "flex")
            }
        })
    });
});

/* This function is used to close the presentation */
function closePresentation () {
    /* Stops the video of part six if it is played */
    if ($("#ytIframe").is(":visible")) {
        stopAndRestartVideo();
    };
    /* Remove the .selected class from the .presentationButtons. */
    $(".selected").removeClass("selected");
    /* Hide the presentation */
    presentation.toggleClass("show");
    /* Show the sidebar icons */
    sidebarIcons.css("visibility", "visible");
};

/* This function is used to close the presentation by clicking on the .closePresentation button. */
$(function () {
    $(".closePresentation").click(function () {
        closePresentation();
    });
});

/* TRANSLATION */
 /* This function makes the #translation part draggable using JQueryUI. */
$(function () {
    translation.draggable({
        /* Use JQueryUI start method to modify css, see https://stackoverflow.com/a/34074800. */
        start: function () {
            $(this).css({
                /* Remove the transform property */
                transform: "none",
                /* The top and left properties are equal to the position of the element in the document from the top and left. */
                top: $(this).offset().top + "px",
                left: $(this).offset().left + "px"
            });
        },
        /* Draggable elements may not extend out of the frame defined by the parent element. */
        containment: "parent"
    });
});

/* SUMMARY */
 /* This function uses an Ajax request to return the contents of the tableOfContents.html file
 * with the lang and currentVerse parameters to generate the table of contents. */
function summary(verse) {
    /* The lang variable is equal to the value of the @lang attribute of the <html> element.
     * By default, this value is equal to "fr". */
    var lang = document.documentElement.lang;
    $.ajax({
        url: "templates/tableOfContents.html",
        method: "get",
        data: {
            /* The currentVerse parameter has the value of the argument of the summary() function. */
            currentVerse: verse,
            /* The lang parameter has the value of the lang variable defined above. */
            lang: lang
        },
        /* The data retrieved is of html type */
        dataType: "html",
        success: function (data) {
            /* If the request is successful, we parse the result */
            var parsedHTML = $.parseHTML(data);
            /* We filter the parsed result of the request to retrieve the .summary and .currentVerse elements. */
            var summary = $(parsedHTML).filter("*").find(".summary");
            var currentVerse = $(parsedHTML).filter("*").find(".currentVerse");
            /* We replace the .summary and .currentVerse elements with those returned by the Ajax request. */
            $(".summary").replaceWith(summary);
            $(".currentVerse").replaceWith(currentVerse);
            if (lang === "ar") {
                $(".summary").css("flex-flow", "row-reverse");
            }
        }
    });
}

/* This function allows access to the first verses of the part selected in the summary. */
$(function () {
    /* On each <option> element change within <select> elements, */
    $(document).on("change", "#selectText, #selectTextPart, #selectTextSubPart", function () {
        /* the value of the verse variable is equal to that of the selected <option> element. */
        var verse = $(this).val()
        /* We find the index number of this verse. */
        var item = getSlideIndexByClass("verse_" + verse);
        /* Then we change the slide to the one corresponding to the index number of the verse. */
        slideTo(item);
    });
});

/* DISPLAY EGYPTIAN TRANSLATION */
 /* This function displays the appropriate button to display or hide the translation of the text. */
function toggletranslationButtonText () {
    if (translation.is(":visible")) {
        /* If the text translation is visible, the .showTranslationButton button is hidden but the .hideTranslationButton button is visible. */
        $(".showTranslationButton").hide();
        $(".hideTranslationButton").show();
    } else {
        /* Conversely, if the text translation is not visible, the .hideTranslationButton button is hidden but the .showTranslationButton button is visible. */
        $(".showTranslationButton").show();
        $(".hideTranslationButton").hide();
    }
};

/* This function allows to toggle the .toggleTransliteration button */
function toggleTransliterationButton () {
    $(".toggleTransliteration").toggle();
};

/* This function displays the appropriate button to display or hide the transliteration of the text. */
function toggleTransliterationButtonText () {
    if ($(".transliteration").is(":visible")) {
        /* If the text transliteration is visible, the .showTransliterationButton button is hidden but the .hideTransliterationButton button is visible. */
        $(".showTransliterationButton").hide();
        $(".hideTransliterationButton").show();
    } else {
        /* Conversely, if the text transliteration is not visible, the .hideTransliterationButton button is hidden but the .showTransliterationButton button is visible. */
        $(".showTransliterationButton").show();
        $(".hideTransliterationButton").hide();
    }
};


/* This function allows to toggle the transliteration which is hidden by default.
 * It also allows to change the text of the .toggleTransliteration button with the toggleTransliterationButtonText() function defined above. */
$(function () {
    $('.toggleTransliteration').click(function () {
        $('.transliteration').toggle();
        toggleTransliterationButtonText();
    });
});

/* This function allows to toggle the .toggleHieroglyph button */
function toggleHieroglyphButton () {
    $(".toggleHieroglyph").toggle();
};

/* This function displays the appropriate button to display or hide the hieroglyphic transcription of the text. */
function toggleHieroglyphButtonText () {
    if ($(".hieroglyphs").is(":visible")) {
        /* If the text transliteration is visible, the .showHieroglyphButton button is hidden but the .hideHieroglyphButton button is visible. */
        $(".showHieroglyphButton").hide();
        $(".hideHieroglyphButton").show();
    } else {
        /* Conversely, if the text transliteration is not visible, the .hideHieroglyphButton button is hidden but the .showHieroglyphButton button is visible. */
        $(".showHieroglyphButton").show();
        $(".hideHieroglyphButton").hide();
    }
};

/* This function allows to toggle the hieroglyphic transcription which is hidden by default.
 * It also allows to change the text of the .toggleHieroglyph button with the toggleHieroglyphButton() function defined above. */
$(function () {
    $('.toggleHieroglyph').click(function () {
        $('.hieroglyphs').toggle();
        if ($(".hieroglyphs").is(":visible")) {
            $(".edition").css("width", "50%")
        } else {
            $(".edition").css("width", "100%")
        }
        toggleHieroglyphButtonText();
    });
});

/* This function allows to toggle the translation of the text. */
function toggleTranslation () {
    if (translation.is(":visible")) {
        /* If the translation is visible,
         * we hide the translation,  */
        translation.hide();
        /* we hide the zone(s) corresponding to the current verse, */
        hideVerseZones();
        /* we hide the .toggleTransliteration button */
        toggleTransliterationButton();
        /* and we change the text of the .translationButton button to that of .showTranslationButton.  */
        toggletranslationButtonText();
        toggleHieroglyphButton();
        toggleHieroglyphButtonText();
    } else {
        /* Conversely, if the translation is not visible,
         * we display the translation, */
        translation.show();
        /* we display the zone(s) corresponding to the current verse, */
        showActiveVerse();
        /* we display the .toggleTransliteration button */
        toggleTransliterationButton();
        /* and we change the text of the .translationButton button to that of .hideTranslationButton. */
        toggletranslationButtonText();
        toggleHieroglyphButton();
        toggleHieroglyphButtonText();
    };
};

/* This function defines what happens when the .translationButton button is clicked. */
$(function () {
    $(".translationButton").click(function () {
        /* If the left sidebar is open, we close it. */
        if (sidebarLeft.hasClass("clicked")) {
            openCloseSidebar(sidebarLeft)
        };
        /* If the audio is playing, we paused it.
         * Thus, when the panel containing the translation is closed, the papyrus reading is also paused. */
        if (! audio.paused) {
            playPauseAudio();
        };
        /* If the panel containing the translation is hidden, it is displayed, otherwise it is hidden. */
        toggleTranslation();
    });
});

/* SWIPER */
 /* This function is used to navigate to the zone(s) corresponding to a verse. */
function moveToVerse(verse) {
    /* We find all zones corresponding to a verse using the getZonesForAVerse class defined in zones.js */
    let zones = zonesManager.getZonesForAVerse(verse);
    if (zones.length >= 2) {
        /* If there is more than one zone, we create a rectangle encompassing the different zones.
         * This rectangle consists of two points, the minimum x of the zones, the minimum y of the zones, of a width and a height */
        let bbox = zones[0].getBoundingBox();
        for (let z of zones) {
            bbox = bbox.merge(z.getBoundingBox());
        };
        /* The viewer adapts to the dimensions of the rectangle. */
        viewer.viewport.fitBounds(new OpenSeadragon.Rect(bbox.getXMin(), bbox.getYMin(), bbox.getWidth(), bbox.getHeight()));
    } else if (zones.length === 1) {
        /* If there is only one zone, the viewer moves and zooms on a point
         * whose x value is equal to the minimum x of the zone added to half its width
         * and the y value is equal to the maximum y of the zone */
        let bbox = zones[0].getBoundingBox();
        var x = bbox.getXMin() +(bbox.getWidth() / 2);
        var y = bbox.getYMax();
        /* Add true to new OpenSeadragon.Point(x,y) for an immediate transition,
         * otherwise the transition is defined by animationTime of const openSeadragonOptions in viewer.js */
        viewer.viewport.panTo(new OpenSeadragon.Point(x, y)).zoomTo(0.0001);
    };
};

/* This function is used to find the class of an element formed on the string "verse_" and thus the number of the verse. */
function getVerse(item) {
    /* We define a regular expression representing the class we want to find. */
    var regex = new RegExp("(verse_)(.{1,})");
    /* We separate the different classes from the item argument of the function/ */
    var itemClasses = item.attr("class").split(/\s+/);
    /* We search among the classes of the item argument of the function that corresponding to the regular expression defined above. */
    var verse = itemClasses.find(function (v) {
        return regex.test(v);
    });
    /* Then we return the value of this class. */
    return verse;
};

/* This function is used to retrieve the index number of a verse in Swiper. */
function getSlideIndexByClass(className) {
    var index = 0;
    $.each($('.swiper-wrapper').children(), function (i, item) {
        /* For each child element of the .swiper-wrapper element,
         * if this element has as class the class used as argument of this function,
         * the value of the variable index is equal to that of its position in the index of child elements of . swiper-wrapper. */
        if ($(item).hasClass(className)) {
            index = i;
            return false;
        };
    });
    return index;
};

/* This function allows to access a slide from its index number. */
function slideTo(item) {
    const swiper = document.querySelector('#textSwiper').swiper;
    /* On swiper.slideTo, see https://swiperjs.com/swiper-api#method-swiper-slideTo */
    swiper.slideTo(item)
};

/* This function allows to navigate to a verse by clicking on
 * the zone corresponding to this verse on the papyrus. */
$(function () {
    $(".zone_verse").click(function () {
        /* Clicking on the area retrieves the verse number */
        var verse = getVerse($(this));
        /* We then find the index number of this verse in swiper. */
        var item = getSlideIndexByClass(verse);
        /* Finally, we access the slide corresponding to this index number. */
        slideTo(item);
    });
});

/* This function is used to hide the zone(s) corresponding to the current verse
 *  by removing the .active_zone_verse class from this zone(s). */
function hideVerseZones() {
    $(".zone_verse").removeClass("active_zone_verse");
};

/* This function is used to display the zone(s) corresponding to the verse used as the argument of this function
 * by adding the .active_zone_verse class to this zone(s) */
function showVerseZones(verse) {
    $(".annotation." + verse).addClass("active_zone_verse");
};

/* This function is used to display the zone(s) corresponding to the current verse. */
function showActiveVerse() {
    /* The current verse is the one with the .swiper-slide-active class. */
    var activeSlide = $(".swiper-slide-active")
    /* If there is a verse with the class .swiper-slide-active. */
    if (activeSlide.length) {
        /* We retrieve the class corresponding to the verse number. */
        var verse = getVerse(activeSlide);
        /* We retrieve the verse number equal to the segment of the string following the underscore character. */
        var verseNumber = verse.substr(verse.indexOf("_") + 1);
        /* We display the zone(s) corresponding to the verse. */
        showVerseZones(verse);
        /* We move to the zone(s) corresponding to the verse. */
        moveToVerse(verseNumber);
        /* We update the summary. */
        summary(verseNumber);
    };
};

/* Swiper configuration */
$(function () {
    const swiper = new Swiper('#textSwiper', {
        /* Direction vertical */
        direction: 'vertical',
        /* No loop needed */
        loop: false,
        /* Previous slide with class .preceding. */
        slidePrevClass: 'preceding',
        /* Next slide with class .following. */
        slideNextClass: 'following',
        /* Only three slides, more would be unsuitable for small screens */
        slidesPerView: 3,
        /* Slides are centered */
        centeredSlides: true,
        /* Pagination in the form of a progress bar in a <div> element with class .swiper-pagination. */
        pagination: {
            el: '.swiper-pagination',
            type: 'progressbar',
            /* The progress bar is horizontal unlike the vertical orientation defined by the direction parameter. */
            progressbarOpposite: true
        },
        /* Allows navigation using the mouse wheel. */
        mousewheel: {
            eventsTarget: '.swiper'
        },
        /* Allows navigation by clicking on a slide. */
        slideToClickedSlide: true,
        on: {
            transitionStart: function () {
                /* When the transition to a new slide begins,
                 * we hide the areas corresponding to the current verse. */
                hideVerseZones ();
            },
            transitionEnd: function () {
                /* When the transition to a new slide ends,
                 * the areas corresponding to the current verse are displayed. */
                showActiveVerse ();
            }
        }
    });
});

/* AUDIO */
/* Play audio */
/* We set up a timer variable for later */
var timer = null;
/* We set up an interval variable whose value is 500 (ms) */
var interval = 500;

/* This function allows to start playing the audio file. */
function playAudio () {
    audio.play();
};

/* This function allows to pause the playback of the audio file. */
function pauseAudio () {
    audio.pause();
};

/* This function is used to display the slide corresponding to a verse when it is read in the audio file. */
function readText () {
    /* The currentTime variable is equal to the current time of the audio file when the function is launched. */
    var currentTime = audio.currentTime;
    const swiper = document.querySelector('#textSwiper').swiper;
    /* Here we use the papyrus definition as JavaScript objects.
     * These objects are defined in the papyrusAsJSObject.js file
     * generated by the XQuery script in the xmlToJSObject.xquery file. */
    for (text of papyrus.textParts) {
        /* For each text of the papyrus */
        if (text.textParts) {
            /* For each text, if there are parts */
            for (textParts of text.textParts) {
                if (textParts.verses) {
                    /* For each part, if there are verses */
                    for (verses of textParts.verses) {
                        /* For each verse, get the start (tBegin) and end value (tEnd) */
                        var tBegin = verses.tBegin;
                        var tEnd = verses.tEnd;
                        if (currentTime <= tEnd && currentTime >= tBegin) {
                            /* If the value of the currentTime variable is less than or equal to that of the verse's end value
                             * and the verse's start value is greater than or equal to the value of the currentTime variable,
                             * we change the slide to the one corresponding to the index of this verse. */
                            swiper.slideTo(getSlideIndexByClass("verse_" + verses.name));
                        };
                    };
                } else {
                    /* Otherwise, for each part, if there are subparts */
                    for (textSubParts of textParts.textParts) {
                        /* For each subpart, if there are verses */
                        for (verses of textSubParts.verses) {
                            /* For each verse, get the start (tBegin) and end value (tEnd) */
                            var tBegin = verses.tBegin;
                            var tEnd = verses.tEnd;
                            if (currentTime <= tEnd && currentTime >= tBegin) {
                                /* If the value of the currentTime variable is less than or equal to that of the verse's end value
                                 * and the verse's start value is greater than or equal to the value of the currentTime variable,
                                 * we change the slide to the one corresponding to the index of this verse. */
                                swiper.slideTo(getSlideIndexByClass("verse_" + verses.name));
                            };
                        };
                    };
                };
            };
        };
    };
};

/* The .playButton button is only visible if the language is "fr". */
function displayAudioButton () {
    var lang = document.documentElement.lang;
    if (lang === "fr") {
        $(".playButton").show();
    } else {
        $(".playButton").hide();
    }
}

$(function () {
   displayAudioButton(); 
});

/* This function allows to start playing the audio file,
 * then replacing this button with a pause button
 * and adding a restart button. */
function playPauseAudio () {
    var icons = $(".playButton").children("i");
    if (audio.paused) {
        /* If the audio is paused, */
         /* show .restartAudioButton, */
        $(".restartAudioButton").toggle();
        /* replace play icon by pause icon, */
        icons.removeClass("bi-play-circle").addClass("bi-pause-circle");
        /* play audio */
        playAudio();
        /* We set up an interval calling the readText function every 500ms.
         * Thus, we change slides and update the summary every 500ms of audio file playback 
         * so that the data displayed corresponds to the text being read. */
        timer = setInterval(readText, interval);
    } else {
        /* If the audio is played, */
         /* hide the .restartAudioButton button, */
        $(".restartAudioButton").toggle();
        /* replace pause icon by play icon, */
        icons.removeClass("bi-pause-circle").addClass("bi-play-circle");
        /* pause audio, */
        pauseAudio();
        /* We stop the execution of the function using the clearInterval method */
        clearInterval(timer);
    };
};

/* This function defines what happens when the user clicks on the .playButton button */
$(function () {
    $(".playButton").click(function () {
        /* If the left sidebar is open, we close it */
        if (sidebarLeft.hasClass("clicked")) {
            openCloseSidebar(sidebarLeft)
        };
        /* If the translation is not visible, we display it. */
        if (! translation.is(":visible")) {
            toggleTranslation();
        };
        /* If the audio is paused, we start it, otherwise we pause it. */
        playPauseAudio();
    });
});

/* This function sets audio current time to 0 */
function restartAudio() {
    audio.currentTime = 0;
};

/* This function is used to restart the audio by setting the current time of the audio to 0
 * when the user clicks the .restartAudioButton button */
$(function () {
    $(".restartAudioButton").click(function () {
        restartAudio();
    });
});

/* VIDEO */
 /* This function is used to stop and restart the YouTube video integrated into the website
 * when the part containing it is no longer displayed. */
function stopAndRestartVideo () {
    $('#ytIframe').each(function (index) {
        /* We replace the @src attribute of the #ytIframe element with its own value.
         * This has the effect of cutting the video and putting it back to the beginning. */
        $(this).attr('src', $(this).attr('src'));
        return false;
    });
};