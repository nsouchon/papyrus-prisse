/**
 * Code for the Openseadragon viewer.
 */

/**
 * Create a full view of a papyrus, by combining its plated.
 * deltaPositions is a list of object with dx,dy properties, which
 * are relative positions for a given plate. with dx,dy at (0,0), 
 * each plate would be placed to the right of the previous plate.
 * @param {*} platesGeometry : dzi files and other information for the plates.
 * @param {number} globalDeltaY : vertical translation applied to all tiles, so that none of them has 
 *                                a negative origin.
 * @param {*} openSeadragonOptions 
 * @returns 
 */
function createFullPapyrusViewer(platesGeometry, globalDeltaY, openSeadragonOptions) {
  // Use them to build the actual tiles.
  let tileSources = []
  let xPos = 0
  for (var i = 0; i < platesGeometry.length; i++) {
    tileSources.push({
      overlays: [],
      x: xPos + platesGeometry[i].dx,
      y: platesGeometry[i].dy + globalDeltaY,
      width: platesGeometry[i].width,
      //height: tile_height,
      tileSource: "data/images/" + platesGeometry[i].dzi
    })
    xPos += platesGeometry[i].width + platesGeometry[i].dx
  }

  // Return the viewer.    
  var viewer = OpenSeadragon({
    tileSources: tileSources,
    columns: 1,
    sequenceMode: false,
    ...openSeadragonOptions
  });
  return viewer; // returns the viewer.
}

/**
 * A coordinate system from multiple plates to single plate...
 */
class MultiPlateGeometry {

  constructor(platesGeometry, globalDeltaY) {
    this.globalDeltaY = globalDeltaY
    this.platesGeometry = platesGeometry;
    /*
    this.plateMap = {}
    for (let i = 0; i < platesGeometry.length; i++) {
        let plate = platesGeometry[i]
        this.plateMap[plate.dzi] = { ...plate }
        this.plateMap[plate.dzi].index = i
    }
     */
  }

  /**
   * Transform in-plate coordinates into global picture coordinates.
   * @param {*} plateIndex 
   * @param {*} x 
   * @param {*} y 
   * @returns 
   */
  fromPlateToMultiplate(plateIndex, x, y) {
    let xr = x
    for (let i = 0; i < plateIndex; i++) {
      let plate = this.platesGeometry[i]
      xr += plate.dx + plate.width
    }
    xr += this.platesGeometry[plateIndex].dx
    let yr = this.globalDeltaY + this.platesGeometry[plateIndex].dy + y
    return { x: xr, y: yr }
  }
}


/**
 * Name Space needed for creating SVG elements.
 */
const svgNameSpace = 'http://www.w3.org/2000/svg';

const openSeadragonOptions = {
  id: "viewer",
  prefixUrl: "iconsFolder/nav/",
  showNavigator: true,
  navigatorId: "navigator",
  showNavigationControl: false,
  minZoomLevel: 0.00007,
  maxZoomLevel: 0.0007,
  defaultZoomLevel: 0.0001,
  visibilityRatio: 0.8,
  constrainDuringPan: true,
  animationTime: 0.5
}

// This function is not supposed to be called.
// It will be removed, and it's just a reminder about technical
// solutions to add overlays. 
function addPrisseOverlays(viewer) {
  // test for a given overlay...
  let overlay = viewer.svgOverlay();
  let d3Rect = d3.select(overlay.node()).append("rect")
    .style('stroke', 'red')
    .style('fill', 'none')
    .style("stroke-width", 100)
    .attr("x", 6488)
    .attr("width", 5467)
    .attr("y", 2778 + globalDeltaY) // correct for picture position... 
    .attr("height", 595);
  //  width: ,
  // height: 
  d3.select(overlay.node()).append("rect")
    .style('stroke', 'red')
    .style('fill', 'none')
    .style("stroke-width", 300)
    .attr("x", 0)
    .attr("width", 18446)
    .attr("y", 0 + globalDeltaY)
    .attr("height", 4893);
  for (let i = 0; i < platesGeometry.length; i++) {
    let p = multiPlateGeometry.fromPlateToMultiplate(i, 0, 0)
    d3.select(overlay.node()).append("rect")
      .style('stroke', 'blue')
      .style('fill', 'none')
      .style("stroke-width", 20)
      .attr("x", p.x)
      .attr("width", 1000)
      .attr("y", p.y) // correct for picture position... 
      .attr("height", 1000);
  }
  // 
  // Planche 4 (ordre inverse) [16793,3210,4309x416@0.05]
  let p = multiPlateGeometry.fromPlateToMultiplate(platesGeometry.length - 1 - 3, 16793, 3210)
  // TODO : Ajouter rotation 
  let cx = p.x + 4309 / 2
  let cy = p.y + 416 / 2
  let angle = 0.05 * 180 / Math.PI
  let rect = document.createElementNS(svgNameSpace, "rect")
  rect.setAttributeNS(null, "style", "stroke: red; stroke-width: 30; fill: transparent;")
  rect.setAttributeNS(null, "x", p.x)
  rect.setAttributeNS(null, "y", p.y)
  rect.setAttributeNS(null, "width", 4309)
  rect.setAttributeNS(null, "height", 416)
  rect.setAttributeNS(null, "transform", `rotate(${angle},${cx},${cy})`)
  overlay.onClick(rect, (e) => alert("début de Ptahotep"))

  overlay.node().appendChild(rect)
  /*
  d3.select(overlay.node()).append("rect")
    .style('stroke', 'red')
    .style('fill', 'transparent')
    .style("stroke-width", 30)
    .attr("x", p.x)
    .attr("width", )
    .attr("y", p.y) // correct for picture position... 
    .attr("height", 416)
    .attr("transform", `rotate(${angle},${cx},${cy})`)
    .on("click", ()=>alert("début de Ptahotep"))
    ;
  */
}

function setupViewer(viewer) {
}

const globalDeltaY = 1000// 930 // Could be computed as minus the min of dy for tiles.
let multiPlateGeometry = new MultiPlateGeometry(platesGeometry, globalDeltaY)
let viewer = createFullPapyrusViewer(platesGeometry, globalDeltaY, openSeadragonOptions)
setupViewer(viewer)

viewer.addHandler('open', function() {
    var zoneInit = new OpenSeadragon.Rect(208211, 114, 11727, 4783)
    viewer.viewport.fitBounds(zoneInit);
});

viewer.zoomPerClick = 1;