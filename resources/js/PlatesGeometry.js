
/**
 * Tile placement information.
 * The whole papyrus is displayed as a very large picture.
 *
 * As plates are not simply displayed side by side, this
 * list contains the slight modification to make to their "normal"
 * position to correctly align the documents.
 */

const platesGeometry =[ {
    dzi: "240l_Egyptien_194.dzi",
    dx: 0,
    dy: 0,
    width: 18446,
    height: 4893
},
// 194
{
    dzi: "240k_Egyptien_193.dzi",
    dx: 0,
    dy: 0,
    width: 12361,
    height: 4753
},
// 193
{
    dzi: "240j_Egyptien_192.dzi",
    dx: 0,
    dy: -115,
    width: 18925,
    height: 4850
},
// 192
{
    dzi: "240i_Egyptien_191.dzi",
    dx: 0,
    dy: -54,
    width: 17906,
    height: 4960
},
// 191
{
    dzi: "240h_Egyptien_190.dzi",
    dx: 0,
    dy: -43,
    width: 17860,
    height: 4993
},
// 190
{
    dzi: "240g_Egyptien_189.dzi",
    dx: 0,
    dy: -35,
    width: 17925,
    height: 4820
},
// 189
{
    dzi: "240f_Egyptien_188.dzi",
    dx: 0,
    dy: -245,
    width: 19233,
    height: 4966
},
// 188
{
    dzi: "240e_Egyptien_187.dzi",
    dx: 0,
    dy: -355,
    width: 12195,
    height: 4760
},
// 187
{
    dzi: "240d_Egyptien_186.dzi",
    dx: 0,
    dy: -530,
    width: 21513,
    height: 4920
},
// 186
{
    dzi: "240c_Egyptien_185.dzi",
    dx: 0,
    dy: -640,
    width: 20893,
    height: 4907
},
// 185
{
    dzi: "240b_Egyptien_184.dzi",
    dx: -25,
    dy: -850,
    width: 20672,
    height: 5104
},
// 184
{
    dzi: "240a_Egyptien_183.dzi",
    dx: 0,
    dy: -930,
    width: 22176,
    height: 4896
}
// 183
]