/* This introduction uses the Intro.js library as part of a non-commercial and open source project.
 * For this library, see https://introjs.com/ */

var intro = introJs();
var steps;
var prevLabel;
var nextLabel;
var doneLabel;
var activeVerse;

/* This function is used to set the value of the element targeted by step 11.
 * If the translation has already been opened, the target element is the area corresponding to the reading location,
 * otherwise the target element is the first area. */
function selectActiveVerse () {
    if ($(".active_zone_verse").length) {
        activeVerse = $(".active_zone_verse")[0];
    } else {
        activeVerse = $("#zone_1")[0];
    }
}

/* This function defines the steps and parameters of the introduction according to the language of the HTML page */
function chooseIntroLanguage () {
    if (html.is(":lang(en)")) {
        prevLabel = "Back"
        nextLabel = "Next"
        doneLabel = "Done"
        steps =[ {
            element: $(".languageButton")[0],
            title: "Change language",
            intro: "Click this button to select the application language.",
            position: "bottom"
        }, {
            element: $(".sidebarLeftButton")[0],
            title: "Left side panel",
            intro: "Click this button to open the left side panel.",
            position: "right"
        }, {
            element: $(".presentationButtons.partOne")[0],
            title: "Show presentation",
            intro: "Click on one of the buttons to display the presentation.",
            position: "right"
        }, {
            element: $(".presentationPart.partOne .closePresentation")[0],
            title: "Close presentation",
            intro: "Click this button to close the presentation.",
            position: "left"
        }, {
            element: $(".sidebarRightButton")[0],
            title: "Right side panel",
            intro: "Click this button to open the right side panel.",
            position: "left"
        }, {
            element: $(".sidebarContent li:first-of-type")[0],
            title: "Show zones",
            intro: "Check one of these boxes to display the corresponding areas.",
            position: "bottom"
        }, {
            element: $(".translationButton")[0],
            title: "Show translation",
            intro: "Click this button to display the translation of the papyrus.",
            position: "right"
        }, {
            element: $(".translationMenu")[0],
            title: "Summary",
            intro: "Here you can select the part of the translation you want to see. You can move the translation panel by clicking here and holding the click.",
            position: "top"
        }, {
            element: $(".toggleTransliteration")[0],
            title: "Show transliteration",
            intro: "Click this button to display the transliteration",
            position: "right"
        }, {
            element: activeVerse,
            title: "Navigate from Papyrus",
            intro: "Click on the papyrus to access the translation of the verse corresponding to the selected area.",
            position: "bottom"
        }, {
            element: $("#navigatorContainer")[0],
            title: "Navigate the papyrus",
            intro: "Use this overview to find your way around and navigate the papyrus.",
            position: "top"
        }, /*{
            element: $(".playButton")[0],
            title: "Text Reading",
            intro: "Click on this button to launch a reading of the French translations of the teachings of Gemnikaï and Ptahhotep.",
            position: "right"
        },*/ {
            element: $(".infoButton")[0],
            title: "Intro",
            intro: "Click this button to start the intro.",
            position: "right"
        }, {
            element: $(".linkGitLab")[0],
            title: "Source code",
            intro: "Click this button to access the source code of the application on GitLab.",
            position: "left"
        }, {
            title: "Contact",
            intro: "You can ask your questions or report problems encountered at the following address <a href='mailto:contactpapyrusprisse@gmail.com'>contactpapyrusprisse@gmail.com</a>."
        }]
    } else if (html.is(":lang(ar)")) {
        prevLabel = "السابق"
        nextLabel = "التالي"
        doneLabel = "تم"
        steps =[ {
            element: $(".languageButton")[0],
            title: "تغيير اللغة",
            intro: "انقر على هذا الزر لتحديد لغة التطبيق.",
            position: "bottom"
        }, {
            element: $(".sidebarLeftButton")[0],
            title: "اللوحة الجانبية اليمنى",
            intro: "انقر على هذا الزر لفتح اللوحة الجانبية اليمنى.",
            position: "right"
        }, {
            element: $(".presentationButtons.partOne")[0],
            title: "إظهار العَرْض",
            intro: "انقر على أحد هذه الأزرار لإظهار العَرْض.",
            position: "right"
        }, {
            element: $(".presentationPart.partOne .closePresentation")[0],
            title: "إغلاق العَرْض",
            intro: "انقر على أحد هذه الأزرار لإغلاق العَرْض.",
            position: "left"
        }, {
            element: $(".sidebarRightButton")[0],
            title: "اللوحة الجانبية اليسرى",
            intro: "انقر على هذا الزر لفتح اللوحة الجانبية اليسرى.",
            position: "left"
        }, {
            element: $(".sidebarContent li:first-of-type")[0],
            title: "إظهار المناطق",
            intro: "اختر إحدى هذه المربعات لإظهار المناطق المقابلة.",
            position: "bottom"
        }, {
            element: $(".translationButton")[0],
            title: "عرض الترجمة",
            intro: "انقر على هذا الزر لعرض ترجمة البردية.",
            position: "right"
        }, {
            element: $(".translationMenu")[0],
            title: "قائمة المحتويات",
            intro: "يمكنك اختيار هنا جزء الترجمة الذي ترغب في الاطلاع عليه. يمكنك تحريك لوحة الترجمة بالنقر هنا مع الاستمرار.",
            position: "top"
        }, {
            element: $(".toggleTransliteration")[0],
            title: "عَرْض الكتابة الصوتية",
            intro: "انقر على هذا الزر لعَرْض الكتابة الصوتية.",
            position: "right"
        }, {
            element: activeVerse,
            title: "التنقل من البردية",
            intro: "انقر على البردية للوصول إلى ترجمة المقولة المقابلة للمنطقة المختارة.",
            position: "bottom"
        }, {
            element: $("#navigatorContainer")[0],
            title: "التنقل على البردية",
            intro: "استخدم هذا الاِسْتِعْراض العام للاستدلال والتنقل على البردية.",
            position: "top"
        }, /*{
            element: $(".playButton")[0],
            title: "قراءة النص",
            intro: "انقر على هذا الزر لبدء قراءة الترجمات الفرنسية لتعاليم جمني كاي وبتاح حتب.",
            position: "right"
        },*/ {
            element: $(".infoButton")[0],
            title: "مقدمة",
            intro: "انقر على هذا الزر لبدء المقدمة.",
            position: "right"
        }, {
            element: $(".linkGitLab")[0],
            title: "الكود الأصلي",
            intro: "انقر على هذا الزر للوصول إلى الكود الأصلي للتطبيق على GitLab.",
            position: "left"
        }, {
            title: "اتصال",
            intro: "يمكنك طرح أسئلتك أو الإبلاغ عن المشكلات التي واجهتها على العنوان التالي <a href='mailto:contactpapyrusprisse@gmail.com'>contactpapyrusprisse@gmail.com</a>."
        }]
    } else {
        prevLabel = "Précédent"
        nextLabel = "Suivant"
        doneLabel = "Terminé"
        steps =[ {
            element: $(".languageButton")[0],
            title: "Changer de langue",
            intro: "Cliquez sur ce bouton pour sélectionner la langue de l'application.",
            position: "bottom"
        }, {
            element: $(".sidebarLeftButton")[0],
            title: "Panneau latéral gauche",
            intro: "Cliquez sur ce bouton pour ouvrir le panneau latéral gauche.",
            position: "right"
        }, {
            element: $(".presentationButtons.partOne")[0],
            title: "Afficher la présentation",
            intro: "Cliquez sur l'un de ces boutons pour afficher la présentation.",
            position: "right"
        }, {
            element: $(".presentationPart.partOne .closePresentation")[0],
            title: "Fermer la présentation",
            intro: "Cliquez sur ce bouton pour fermer la présentation.",
            position: "left"
        }, {
            element: $(".sidebarRightButton")[0],
            title: "Panneau latéral droit",
            intro: "Cliquez sur ce bouton pour ouvrir le panneau latéral droit.",
            position: "left"
        }, {
            element: $(".sidebarContent li:first-of-type")[0],
            title: "Afficher les zones",
            intro: "Cochez l'une de ces cases pour afficher les zones correspondantes.",
            position: "bottom"
        }, {
            element: $(".translationButton")[0],
            title: "Afficher la traduction",
            intro: "Cliquez sur ce bouton pour afficher la traduction du papyrus.",
            position: "right"
        }, {
            element: $(".translationMenu")[0],
            title: "Sommaire",
            intro: "Vous pouvez sélectionner ici la partie de la traduction que vous souhaitez consulter. Vous pouvez déplacer le panneau de traduction en cliquant ici et en maintenant le clic.",
            position: "top"
        }, {
            element: $(".toggleTransliteration")[0],
            title: "Afficher la translittération",
            intro: "Cliquez sur ce bouton pour afficher la translittération.",
            position: "right"
        }, {
            element: activeVerse,
            title: "Naviguer depuis le papyrus",
            intro: "Cliquez sur le papyrus pour accéder à la traduction du vers correspondant à la zone sélectionnée.",
            position: "bottom"
        }, {
            element: $("#navigatorContainer")[0],
            title: "Naviguer sur le papyrus",
            intro: "Utilisez cette vue générale pour vous repérer et naviguer sur le papyrus.",
            position: "top"
        }, {
            element: $(".playButton")[0],
            title: "Lecture du texte",
            intro: "Cliquez sur ce bouton pour lancer une lecture des traductions françaises des enseignements de Gemnikaï et de Ptahhotep.",
            position: "right"
        }, {
            element: $(".infoButton")[0],
            title: "Introduction",
            intro: "Cliquez sur ce bouton pour lancer l'introduction.",
            position: "right"
        }, {
            element: $(".linkGitLab")[0],
            title: "Source code",
            intro: "Cliquer sur ce bouton pour accéder au code source de l'application sur GitLab.",
            position: "left"
        }, {
            title: "Contact",
            intro: "Vous pouvez poser vos questions ou signaler des problèmes rencontrés à l’adresse suivante <a href='mailto:contactpapyrusprisse@gmail.com'>contactpapyrusprisse@gmail.com</a>."
        }]
    }
}

/* This function defines the delay between two steps.
 * It allows to correctly target elements whose position changes 
 * between the start of the introduction and the step where they are displayed.
 * See https://introjs.com/docs/examples/advanced/async-await */
function delay (t) {
    return new Promise((resolve) => {
        setInterval(resolve, t);
    });
}

$(".infoButton").on("click", function () {
    /* Set the value of the activeVerse element */
    selectActiveVerse();
    /* Select steps and parameters according to the language of the HTML page */
    chooseIntroLanguage();
    /* Close the left side panel if it is open */
    if (sidebarLeft.hasClass("clicked")) {
        openCloseSidebar(sidebarLeft);
    };
    /* Close the right side panel if it is open */
    if (sidebarRight.hasClass("clicked")) {
        openCloseSidebar(sidebarRight);
    };
    /* Pause audio file playback if playing */
    if (! audio.paused) {
        playPauseAudio();
    }
    /* Hide the translation panel if it is visible */
    if (translation.is(":visible")) {
        toggleTranslation();
    }
    /* Hide the toggleTransliteration button if it is visible */
    if ($(".toggleTransliteration").is(":visible")) {
        toggleTransliterationButton();
    }
    intro.setOptions({
        /* Do not show introduction bullets */
        showBullets: false,
        /* These options are set by the chooseIntroLanguage() function */
        steps: steps,
        prevLabel: prevLabel,
        nextLabel: nextLabel,
        doneLabel: doneLabel
    });
    intro.onbeforechange(function () {
        /* Before each change, execute the different functions necessary for the correct display of the elements
         * according to the current step and the direction of the introduction */
        var currentStep = this._currentStep;
        var direction = this._direction;
        if (direction === "forward") {
            switch (currentStep) {
                case 2:
                    /* Open the left side panel */
                    openCloseSidebar(sidebarLeft);
                    /* then perform the introduction step after 600ms */
                    return delay(600);
                case 3:
                    /* Open the presentation panel */
                    togglePresentation();
                    /* Show the first part of the presentation */
                    showPresentationPart($(".presentationButtons.partOne"));
                    /* then perform the introduction step after 1s */
                    return delay(1000);
                case 4:
                    /* Close the presentation panel */
                    closePresentation();
                    /* Close the left side panel */
                    openCloseSidebar(sidebarLeft);
                    /* then perform the introduction step after 600ms */
                    return delay(600);
                case 5:
                    /* Open the right side panel */
                    openCloseSidebar(sidebarRight);
                    /* then perform the introduction step after 600ms */
                    return delay(600);
                case 6:
                    /* Close the right side panel */
                    openCloseSidebar(sidebarRight);
                    /* then perform the introduction step after 600ms */
                    return delay(600);
                case 7:
                    /* Show the translation panel */
                    toggleTranslation();
                    /* then perform the introduction step after 600ms */
                    return delay(600);
                case 10:
                    /* Hide the translation panel */
                    toggleTranslation();
                    break;
                default:
                    break;
            }
        } else {
            console.log(currentStep);
            switch (currentStep) {
                case 1:
                    /* Close the left side panel */
                    openCloseSidebar(sidebarLeft);
                    /* then perform the introduction step after 600ms */
                    return delay(600);
                case 2:
                    /* Hide the first part of the presentation */
                    presentationPart.hide();
                    /* Close the presentation panel */
                    closePresentation();
                    break;
                case 3:
                    /* Open the left side panel */
                    openCloseSidebar(sidebarLeft);
                    /* Open the presentation panel */
                    togglePresentation();
                    /* Show the first part of the presentation */
                    showPresentationPart($(".presentationButtons.partOne"));
                    /* then perform the introduction step after 1s */
                    return delay(1000);
                case 4:
                    /* Close the right side panel */
                    openCloseSidebar(sidebarRight);
                    /* then perform the introduction step after 600ms */
                    return delay(600);
                case 5:
                    /* Open the right side panel */
                    openCloseSidebar(sidebarRight);
                    /* then perform the introduction step after 600ms */
                    return delay(600);
                case 6:
                    /* Hide the translation panel */
                    toggleTranslation();
                    break;
                case 9:
                    /* Show the translation panel */
                    toggleTranslation();
                    break;
                default:
                    break;
            }
        }
    });
    intro.start();
});