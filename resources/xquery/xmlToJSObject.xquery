xquery version "3.1";

declare default element namespace "http://www.tei-c.org/ns/1.0";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "text";

(: This script is used to generate a file containing the description of the papyrus in the form of JavaScript objects. 
These objects are defined in the objectsDeclaration.js file. :)

declare variable $annotation := doc("/db/apps/pprisse/data/xml/2022_pPrisse_Alignement_Audio_Francais.xml");
declare variable $edition := doc("/db/apps/pprisse/data/xml/2022_pPrisse_Edition.xml");

(: Declaration of JavaScript objects.
Each text has a name, sometimes parts and sometimes verses.
Each part has a name, sometimes subparts and sometimes verses.
Each sub-part has a name and verses. 
Finally, each verse has a name, a start time (tBegin) and an end time (tEnd). :)
let $objectDefinition := "function texts(name, textParts, verses) {this.name = name;this.textParts = textParts;this.verses = verses;};function parts(name, textParts, verses) {this.name = name;this.textParts = textParts;this.verses = verses;};function subparts(name, verses) {this.name = name;this.verses = verses;};function verses(name, tBegin, tEnd) {this.name = name;this.tBegin = tBegin;this.tEnd = tEnd;};"

(: The papyrus object is composed of a name and several texts that themselves have a name (Here the value of the @n attribute of the <div @type="text"> elements). :)
let $papyrus := let $texts := $edition/descendant::div[@type="translation" and @xml:lang="fr"]/child::div[@type="text"]/@n
                         return
                            concat("let papyrus = { name: 'Papyrus Prisse',textParts:[",string-join($texts, ","),"]};")

(: Each text object has a name (here the value of the @n attribute of each <div @type="text"> element) 
and one or more parts (<div @type="part">) which themselves have a name (here the value of the @n attribute of the <div @type="part"> elements children of the <div @type="text"> element). 
Some texts have no parts, only verses that also have a name (here the @n attribute of each <l> child element of the <div @type="part">). :)
let $texts := for $text in $edition/descendant::div[@type="translation" and @xml:lang="fr"]/child::div[@type="text"]
                    let $name := $text/@n
                    let $parts := $text/child::div[@type="part"]/@n
                    let $verses := for $verses in $text/descendant::l/@n
                                           return
                                                translate($verses,  '-', '')
                    return
                        if ($parts)
                        then
                            concat('let ', $name, ' = new texts("', $name, '",[', string-join($parts, ","), '],null);')
                        else
                            concat('let ', $name, ' = new texts("', $name, '",null,[', string-join($verses, ","), ']);')

(: Each part object has a name (here the value of the @n attribute of each <div @type="part"> element) 
and one or more subparts (<div @type="subpart">) having same a name (here the value of the @n attribute of the <div @type="subpart"> elements children of the <div @type="part"> element).
Some parts have no subparts, only verses that also have a name (here the @n attribute of each child <l> element of the <div @type="part">). :)
let $textParts := for $textpart in $edition/descendant::div[@type="translation" and @xml:lang="fr"]/descendant::div[@type="part"]
                let $name := $textpart/@n
                let $subparts := $textpart/child::div[@type="subpart"]/@n
                let $verses := for $verses in $textpart/descendant::l/@n
                                      return
                                        translate($verses,  '-', '')
                return
                    if ($subparts)
                    then
                        concat('let ', $name, ' = new parts("', $name, '",[', string-join($subparts, ","), '],null);')
                    else
                        concat('let ', $name, ' = new parts("', $name, '",null,[', string-join($verses, ","), ']);')

(: Each subpart object has a name (here the value of the @n attribute of each <div @type="subpart"> element) 
and one or more verses also having a name (here the @n attribute of each child <l> element of the <div @type="subpart">). :)
let $textSubParts := for $textSubPart in $edition/descendant::div[@type="translation" and @xml:lang="fr"]/descendant::div[@type="subpart"]
                let $name := $textSubPart/@n
                let $verses := for $verses in $textSubPart/descendant::l/@n
                                      return
                                        translate($verses,  '-', '')
                return
                    concat('let ', $name, ' = new subparts("', $name, '",[', string-join($verses, ","), ']);')

(: Each verse has a name (here the value of the @n attribute of each <l> element), 
a start value (here the value of the sum of all the values of the @interval attributes of the <l> elements preceding the current <l> element) 
and an end value (here the sum of the start value and that of the @interval attribute of the current <l> element). :)
let $verses := for $l in $edition/descendant::div[@type="translation" and @xml:lang="fr"]/descendant::l
                let $n := $l/@n
                let $name := translate($n, '-', '')
                let $when := $annotation/descendant::when[@xml:id=concat("TL_", $n)]
                let $start := sum($when/preceding::when/@interval)
                let $end := $start+$when/@interval
                return
                    if ($when)
                    then
                        concat('let ', $name, ' = new verses("', $n, '", ', $start, ', ', $end, ');')
                    else
                        (: Some unread verses in the audio file do not have a start and end value, like the verses belonging to the erased text :)
                        concat('let ', $name, ' = new verses("', $n, '", null, null);')

(: papyrusAsJSObject.js is the sum of all previous functions combined into a single file. :)
let $object := $objectDefinition || serialize($verses) || serialize($textSubParts) || serialize($textParts) || serialize($texts) || serialize($papyrus)
return
    xmldb:store(
        '/db/apps/pprisse/resources/js',
        'papyrusAsJSObject.js',
        serialize($object)
    )