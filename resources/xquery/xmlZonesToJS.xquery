xquery version "3.1";

declare default element namespace "http://www.tei-c.org/ns/1.0";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json = "http://www.json.org";
declare option output:method "json";
declare option output:media-type "application/json";

(: This script is used to convert XML-encoded zone data to JSON. 
This JSON is saved in the database as a JavaScript file including a constant zonesJSON equal to the JSON data.
This zonesJSON constant is used in the zones.js file to create the zones to be displayed.
Each <zone> XML element corresponds to a photograph in tif format, replaced here by the dzi file corresponding to this photograph.
Each <zone> has an @type attribute corresponding to the zone type (verse, jointleft, jointright, polyptoton, palimpsest, rubric).
Each <zone> has an @points attribute whose value is the x and y coordinates of each point forming the zone.
Each value of this @points attribute takes the form of a string composed by the x value followed by a comma and the y value.
Finally, each <zone> has an @ana attribute corresponding to the verse number and an @xml:id attribute giving it a unique identifier. :)

let $doc := doc("/db/apps/pprisse/data/xml/2022_pPrisse_Edition.xml")
let $json :=
    serialize(array {$doc/descendant::surface !
        map {
            "dzi":string(replace(graphic/@url, "(tif)", "dzi")),
            "zones":
                array {zone !
                    map {
                        "zoneType":string(@type),
                        "points":
                            array {tokenize(@points) !
                                map {
                                    "x":number(substring-before(., ",")),
                                    "y":number(substring-after(., ","))
                                }
                            },
                        "idx":string(@xml:id),
                        "data": map {
                            "verse":string(@ana)
                        }
                    }
                }
        }
    },
    map{"method":"json"}
    )
return
    xmldb:store(
        '/db/apps/pprisse/resources/js',
        'zonesDefinitions.js',
        concat("const zonesJSON = ", $json)
    )