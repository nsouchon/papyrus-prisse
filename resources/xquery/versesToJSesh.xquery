xquery version "3.1";

declare default element namespace "http://www.tei-c.org/ns/1.0";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "text";

let $xslt := doc("/db/apps/pprisse/resources/xslt/versesToJSesh.xsl")
let $edition := doc("/db/apps/pprisse/data/xml/2022_pPrisse_Edition.xml")
let $hieroglyphicEncoding := $edition/descendant::div[@type="hieroglyphicEncoding"]
let $text := transform:transform($hieroglyphicEncoding, $xslt, ())
return
    xmldb:store(
        '/db/apps/pprisse/resources/jsesh',
        'papyrusPrisse.gly',
        $text/string()
    )