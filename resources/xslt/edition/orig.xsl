<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xs="http://www.w3.org/2001/XMLSchema" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
    
    <xsl:template match="orig">
        <xsl:choose>
            <xsl:when test="child::text()">
                <span class="editorialMarks">
                    <xsl:text>{</xsl:text>
                </span>
                <xsl:apply-templates/>
                <span class="editorialMarks">
                    <xsl:text>}</xsl:text>
                </span>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>