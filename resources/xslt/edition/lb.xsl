<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xs="http://www.w3.org/2001/XMLSchema" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
    
    <xsl:template match="lb">
        <xsl:choose>
            <xsl:when test="ancestor::div[@type='edition']">
                <xsl:if test="preceding-sibling::node()">
                    <xsl:text> </xsl:text>
                </xsl:if>
                <span class="editorialMarks">
                    <xsl:variable name="n" select="@n"/>
                    <xsl:text>(</xsl:text>
                    <xsl:number value="preceding::cb[1]/@n" format="I"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="@n"/>
                    <xsl:text>)</xsl:text>
                </span>
                <xsl:if test="following-sibling::node()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="preceding-sibling::node()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>