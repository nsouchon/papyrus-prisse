<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xs="http://www.w3.org/2001/XMLSchema" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
    
    <xsl:template match="del">
        <xsl:choose>
            <xsl:when test="parent::l">
                <span class="editorialMarks">
                    <xsl:choose>
                        <xsl:when test="@rend='erasure'">
                            <xsl:text> ⟦</xsl:text>
                        </xsl:when>
                    </xsl:choose>
                    <xsl:apply-templates/>
                    <xsl:choose>
                        <xsl:when test="@rend='erasure'">
                            <xsl:text>⟧ </xsl:text>
                        </xsl:when>
                    </xsl:choose>
                </span>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>