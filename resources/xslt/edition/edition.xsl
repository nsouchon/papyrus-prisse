<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xs="http://www.w3.org/2001/XMLSchema" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
    
    <xsl:import href="body.xsl"/>
    <xsl:import href="head.xsl"/>
    <xsl:import href="l.xsl"/>
    <xsl:import href="lb.xsl"/>
    <xsl:import href="supplied.xsl"/>
    <xsl:import href="choice.xsl"/>
    <xsl:import href="orig.xsl"/>
    <xsl:import href="reg.xsl"/>
    <xsl:import href="sic.xsl"/>
    <xsl:import href="corr.xsl"/>
    <xsl:import href="surplus.xsl"/>
    <xsl:import href="del.xsl"/>
    <xsl:import href="gap.xsl"/>
    
    <!-- This parameter is defined by the transform:transform function in the app.xqm file (function app:translation) 
        and is used to select the translation of the papyrus in the language chosen by the user -->
    <xsl:param name="lang"/>
    
    <xsl:template match="/">
        <xsl:apply-templates>
            <xsl:with-param name="lang"/>
        </xsl:apply-templates>
    </xsl:template>
    
</xsl:stylesheet>