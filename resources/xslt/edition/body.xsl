<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xs="http://www.w3.org/2001/XMLSchema" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
    
    <xsl:template match="body">
        <xsl:for-each select="descendant::div[@type='edition']/descendant::l">
            <xsl:variable name="n" select="@n"/>
            <xsl:variable name="corresp" select="@corresp"/>
            <xsl:variable name="ln" select="substring-after($corresp, '#')"/>
            <div>
                <xsl:attribute name="class">
                    <xsl:text>swiper-slide </xsl:text>
                    <xsl:value-of select="concat('verse_', $n)"/>
                </xsl:attribute>
                <div class="hieroglyphs">
                    <img>
                        <xsl:attribute name="src">
                            <xsl:value-of select="concat('$mydata/hieroglyphs/', $ln, '.svg')"/>
                        </xsl:attribute>
                        <xsl:attribute name="alt">
                            <xsl:value-of select="$ln"/>
                        </xsl:attribute>
                        <xsl:attribute name="loading">
                            <xsl:text>lazy</xsl:text>
                        </xsl:attribute>
                    </img>
                </div>
                <div class="edition">
                    <div class="transliteration">
                        <xsl:apply-templates/>
                    </div>
                    <div class="translation">
                        <xsl:attribute name="data-i18n">
                            <xsl:value-of select="concat('verse_', $ln)"/>
                        </xsl:attribute>
                        <!-- The $lang parameter is defined in the edition.xsl file according to the parameter passed to this file by the transform:transform function defined in the app.xqm file (function app:translation) -->
                        <xsl:apply-templates select="following::div[@type='translation' and @xml:lang=$lang]/descendant::l[@corresp=$corresp]"/>
                    </div>
                </div>
            </div>
        </xsl:for-each>
    </xsl:template>
    
</xsl:stylesheet>