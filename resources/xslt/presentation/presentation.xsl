<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xs="http://www.w3.org/2001/XMLSchema" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
    
    <xsl:import href="div.xsl"/>
    <xsl:import href="head.xsl"/>
    <xsl:import href="table.xsl"/>
    <xsl:import href="list.xsl"/>
    <xsl:import href="p.xsl"/>
    <xsl:import href="hi.xsl"/>
    <xsl:import href="figure.xsl"/>
    <xsl:import href="graphic.xsl"/>
    <xsl:import href="media.xsl"/>
    <xsl:import href="ref.xsl"/>
    
    <!-- This parameter is defined by the transform:transform function in the app.xqm file (function app:presentation) 
        and is used to display the copyright translation corresponding to the language selected by the user -->
    <xsl:param name="lang"/>
    
    <xsl:template match="/">
        <xsl:apply-templates>
            <xsl:with-param name="lang"/>
        </xsl:apply-templates>
    </xsl:template>
    
</xsl:stylesheet>