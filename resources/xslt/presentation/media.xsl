<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xs="http://www.w3.org/2001/XMLSchema" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
    
    <xsl:template match="media">
        <xsl:choose>
            <xsl:when test="contains(@url, 'youtube')">
                <iframe>
                    <xsl:attribute name="id">
                        <xsl:text>ytIframe</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="width">
                        <xsl:text>560</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="height">
                        <xsl:text>315</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="src">
                        <xsl:value-of select="@url"/>
                    </xsl:attribute>
                    <xsl:attribute name="title">
                        <xsl:text>YouTube video player</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="allow">
                        <xsl:text>accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="allowfullscreen"/>
                    <xsl:attribute name="loading">
                        <xsl:text>lazy</xsl:text>
                    </xsl:attribute>
                </iframe>
            </xsl:when>
            <xsl:otherwise>
                <video>
                    <source>
                        <xsl:attribute name="type">
                            <xsl:value-of select="@mimeType"/>
                        </xsl:attribute>
                        <xsl:attribute name="src">
                            <xsl:value-of select="concat('$mydata/illustrations/', @url)"/>
                        </xsl:attribute>
                    </source>
                </video>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>