<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xs="http://www.w3.org/2001/XMLSchema" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
    
    <xsl:template match="head">
        <xsl:choose>
            <xsl:when test="parent::figure">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:when test="parent::list">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:when test="parent::table">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <h2>
                    <xsl:attribute name="data-i18n">
                        <xsl:value-of select="concat(ancestor::div[@type='part']/@n, '_h', position())"/>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </h2>
                <button class="closePresentation">
                    <i class="bi bi-x"/>
                </button>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>