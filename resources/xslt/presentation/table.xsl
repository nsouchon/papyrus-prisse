<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xs="http://www.w3.org/2001/XMLSchema" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
    
    <xsl:template match="table">
        <xsl:variable name="table" select="position()"/>
        <xsl:if test="head">
            <h2>
                <xsl:apply-templates select="head"/>
            </h2>
        </xsl:if>
        <table>
            <xsl:attribute name="id">
                <xsl:value-of select="concat(ancestor::div[@type='part']/@n, '_t', $table)"/>
            </xsl:attribute>
            <xsl:for-each select="row">
                <xsl:variable name="row" select="position()"/>
                <tr>
                    <xsl:for-each select="cell">
                        <xsl:variable name="cell" select="position()"/>
                        <td>
                            <xsl:if test="ancestor::div[@type='part' and @n='partSix'] and $row=1">
                                <xsl:attribute name="colspan">
                                    <xsl:number value="3"/>
                                </xsl:attribute>
                            </xsl:if>
                            <xsl:attribute name="data-i18n">
                                <xsl:value-of select="concat(ancestor::div[@type='part']/@n, '_t', $table,  '_r', $row,  '_c', $cell)"/>
                            </xsl:attribute>
                            <xsl:apply-templates/>
                        </td>
                    </xsl:for-each>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
    
</xsl:stylesheet>