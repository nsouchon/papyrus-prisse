<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:expath="http://expath.org/ns/pkg" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:repo="http://exist-db.org/xquery/repo" xmlns:xs="http://www.w3.org/2001/XMLSchema" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
    
    <xsl:variable name="repo" select="doc('../../../repo.xml')"/>
    <xsl:variable name="expath-pkg" select="doc('../../../expath-pkg.xml')"/>
    <xsl:variable name="version">
        <xsl:value-of select="$expath-pkg/expath:package/@version"/>
    </xsl:variable>
    <xsl:variable name="versionDate">
        <xsl:value-of select="$repo/descendant::repo:deployed"/>
    </xsl:variable>
    
    <xsl:template match="div[@type='presentation']">
        <xsl:for-each select="child::div[@type='part' and @n!='introduction']">
            <div>
                <xsl:attribute name="class">
                    <xsl:text>presentationPart </xsl:text>
                    <xsl:value-of select="@n"/>
                </xsl:attribute>
                <div class="presentationHead">
                    <xsl:apply-templates select="head"/>
                </div>
                <div class="presentationBody">
                    <xsl:apply-templates select="*[local-name()!='head']"/>
                    <xsl:if test="@n='acknowledgements'">
                        <hr/>
                        <div class="projectContact">
                            <h2>
                                <xsl:choose>
                                    <xsl:when test="$lang='fr'">
                                        <h2 data-i18n="contactTitle">Contact :</h2>
                                    </xsl:when>
                                    <xsl:when test="$lang='en'">
                                        <h2 data-i18n="contactTitle">Contact:</h2>
                                    </xsl:when>
                                    <xsl:when test="$lang='ar'">
                                        <h2 data-i18n="contactTitle">اِتِّصال:</h2>
                                    </xsl:when>
                                </xsl:choose>
                            </h2>
                            <xsl:choose>
                                <xsl:when test="$lang='fr'">
                                    <p data-i18n="contact">Vous pouvez poser vos questions ou signaler des problèmes rencontrés à l’adresse suivante <a href="mailto:contactpapyrusprisse@gmail.com">contactpapyrusprisse@gmail.com</a>.</p>
                                </xsl:when>
                                <xsl:when test="$lang='en'">
                                    <p data-i18n="contact">You can ask your questions or report problems encountered at the following address <a href="mailto:contactpapyrusprisse@gmail.com">contactpapyrusprisse@gmail.com</a>.</p>
                                </xsl:when>
                                <xsl:when test="$lang='ar'">
                                    <p data-i18n="contact">يمكنك طرح أسئلتك أو الإبلاغ عن المشاكل التي واجهتها على العنوان التالي <a href="mailto:contactpapyrusprisse@gmail.com">contactpapyrusprisse@gmail.com</a>.</p>
                                </xsl:when>
                            </xsl:choose>
                        </div>
                        <hr/>
                        <div class="projectCopyright">
                            <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png"/></a>
                            <!-- The $lang parameter is defined in the presentation.xsl file according to the parameter passed to this file by the transform:transform function defined in the app.xqm file (function app:presentation) -->
                            <xsl:choose>
                                <xsl:when test="$lang='fr'">
                                    <p data-i18n="licence">Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</a>.</p>
                                </xsl:when>
                                <xsl:when test="$lang='en'">
                                    <p data-i18n="licence">This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.</p>
                                </xsl:when>
                                <xsl:when test="$lang='ar'">
                                    <p data-i18n="licence">هذا العمل متاح بموجب أحكام <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.</p>
                                </xsl:when>
                            </xsl:choose>
                        </div>
                        <hr/>
                        <div class="version">
                            <div>
                                <xsl:text>Version </xsl:text>
                                <xsl:value-of select="$version/string()"/>
                            </div>
                            <div>
                                <xsl:value-of select="format-dateTime($versionDate, '[Y0001]-[M01]-[D01]')"/>
                            </div>
                        </div>
                    </xsl:if>
                </div>
            </div>
        </xsl:for-each>
    </xsl:template>
    
</xsl:stylesheet>