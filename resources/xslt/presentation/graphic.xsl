<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xs="http://www.w3.org/2001/XMLSchema" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
    
    <xsl:template match="graphic">
        <xsl:choose>
            <xsl:when test="contains(@url, 'http')">
                <a>
                    <xsl:attribute name="href">
                        <xsl:value-of select="child::desc[@type='source']"/>
                    </xsl:attribute>
                    <xsl:attribute name="target">
                        <xsl:text>_blank</xsl:text>
                    </xsl:attribute>
                    <img>
                        <xsl:if test="desc[@type='type' and child::text()]">
                            <xsl:attribute name="class">
                                <xsl:value-of select="desc[@type='type']"/>
                            </xsl:attribute>
                        </xsl:if>
                        <xsl:attribute name="src">
                            <xsl:choose>
                                <xsl:when test="contains(@url, 'http')">
                                    <xsl:value-of select="@url"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="concat('$mydata/illustrations/', @url)"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                        <xsl:attribute name="alt">
                            <xsl:value-of select="desc[@type='title']"/>
                        </xsl:attribute>
                        <xsl:attribute name="loading">
                            <xsl:text>lazy</xsl:text>
                        </xsl:attribute>
                    </img>
                </a>
            </xsl:when>
            <xsl:otherwise>
                <img>
                    <xsl:if test="desc[@type='type' and child::text()]">
                        <xsl:attribute name="class">
                            <xsl:value-of select="desc[@type='type']"/>
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:attribute name="src">
                        <xsl:choose>
                            <xsl:when test="contains(@url, 'http')">
                                <xsl:value-of select="@url"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="concat('$mydata/illustrations/', @url)"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:attribute name="alt">
                        <xsl:value-of select="desc[@type='title']"/>
                    </xsl:attribute>
                    <xsl:attribute name="loading">
                        <xsl:text>lazy</xsl:text>
                    </xsl:attribute>
                </img>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>