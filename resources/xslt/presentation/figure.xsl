<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xs="http://www.w3.org/2001/XMLSchema" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
    
    <xsl:template match="figure">
        <xsl:choose>
            <xsl:when test="child::media[contains(@url, 'youtube')]">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <figure>
                    <xsl:choose>
                        <xsl:when test="child::graphic">
                            <xsl:apply-templates select="graphic"/>
                        </xsl:when>
                        <xsl:when test="child::media">
                            <xsl:apply-templates select="media"/>
                        </xsl:when>
                    </xsl:choose>
                    <figcaption>
                        <div class="caption">
                            <xsl:attribute name="data-i18n">
                                <xsl:value-of select="concat(ancestor::div[@type='part']/@n, '_f', position())"/>
                            </xsl:attribute>
                            <xsl:apply-templates select="head"/>
                        </div>
                        <xsl:choose>
                            <xsl:when test="child::graphic[child::desc[@type='copyright'] and contains(@url, 'http')]">
                                <div class="copyright">
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="child::graphic/child::desc[@type='source']"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="target">
                                            <xsl:text>_blank</xsl:text>
                                        </xsl:attribute>
                                        <xsl:text>© </xsl:text>
                                        <xsl:value-of select="child::graphic/child::desc[@type='copyright']"/>
                                    </a>
                                </div>
                            </xsl:when>
                            <xsl:otherwise/>
                        </xsl:choose>
                    </figcaption>
                </figure>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>