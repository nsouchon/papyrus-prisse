<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xs="http://www.w3.org/2001/XMLSchema" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
   
   <xsl:template match="list">
      <xsl:choose>
         <xsl:when test="@n='partners'">
            <hr/>
            <h2>
               <xsl:attribute name="data-i18n">
                  <xsl:value-of select="concat(ancestor::div[@type='part']/@n, '_l', position())"/>
               </xsl:attribute>
               <xsl:apply-templates select="head"/>
            </h2>
            <div class="logoPartners">
               <xsl:for-each select="item">
                  <img>
                     <xsl:attribute name="src">
                        <xsl:value-of select="concat('$myresources/images/logo_partners/logo_', @n ,'.png')"/>
                     </xsl:attribute>
                     <xsl:attribute name="alt">
                        <xsl:value-of select="concat('Logo', text())"/>
                     </xsl:attribute>
                  </img>
               </xsl:for-each>
            </div>
         </xsl:when>
         <xsl:when test="@n='webHosting'">
            <hr/>
            <h2>
               <xsl:attribute name="data-i18n">
                  <xsl:value-of select="concat(ancestor::div[@type='part']/@n, '_l', position())"/>
               </xsl:attribute>
               <xsl:apply-templates select="head"/>
            </h2>
            <div class="logoWebHosting">
               <xsl:for-each select="item">
                  <img>
                     <xsl:attribute name="src">
                        <xsl:value-of select="concat('$myresources/images/logo_Huma-Num/logo_', @n ,'.png')"/>
                     </xsl:attribute>
                     <xsl:attribute name="alt">
                        <xsl:value-of select="concat('Logo_', text())"/>
                     </xsl:attribute>
                  </img>
               </xsl:for-each>
            </div>
            <hr/>
         </xsl:when>
         <xsl:otherwise>
            <h2>
               <xsl:attribute name="data-i18n">
                  <xsl:value-of select="concat(ancestor::div[@type='part']/@n, '_l', position())"/>
               </xsl:attribute>
               <xsl:apply-templates select="head"/>
            </h2>
            <ul>
               <xsl:variable name="list" select="position()"/>
               <xsl:for-each select="item">
                  <xsl:variable name="item" select="position()"/>
                  <li>
                     <xsl:attribute name="data-i18n">
                        <xsl:value-of select="concat(ancestor::div[@type='part']/@n, '_l', $list, '_i', $item)"/>
                     </xsl:attribute>
                     <xsl:apply-templates/>
                  </li>
               </xsl:for-each>
            </ul>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   
</xsl:stylesheet>