<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
    
    <xsl:output method="text"/>
    <xsl:output omit-xml-declaration="yes"/>
    <xsl:output xml:space="default"/>
    <xsl:output indent="no"/>
    
    <xsl:template match="div[@type='hieroglyphicEncoding']">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="div[@type='text']">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="ab">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="l">
        <xsl:variable name="verseNumber" select="@n"/>
        <xsl:text>|</xsl:text>
        <xsl:value-of select="replace($verseNumber, '(-)', '\\$1')"/>
        <xsl:text>-</xsl:text>
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="lb">
        <xsl:if test="preceding-sibling::text()">
            <xsl:text>-</xsl:text>
        </xsl:if>
    </xsl:template>
    <xsl:template match="cb"/>
    <xsl:template match="del">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="gap">
        <xsl:choose>
            <xsl:when test="@reason='lost' and @unit='quadrates'">
                <xsl:text>-//-//-//</xsl:text>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>
    
</xsl:stylesheet>