# Papyrus Prisse

## Présentation

L’application Papyrus Prisse a été développée dans le cadre du [programme Écritures](https://www.ifao.egnet.net/recherche/operations/op19225/). Elle permet de parcourir et d’interagir avec une reconstitution numérique du papyrus, d’afficher la transcription hiéroglyphique, la translittération et la traduction – en français et en anglais – de ses textes, d’en écouter une lecture et d’afficher différentes présentations portant tant sur le papyrus et son histoire que sur le type d’écriture utilisée ou les particularités de sa mise en page.

Cette application web monopage a été développée et déployée en utilisant [eXist-db](http://exist-db.org/exist/apps/homepage/index.html). Les données encodées au format XML/TEI-EpiDoc sont formatées par le biais de requêtes XQuery et de transformations XSLT et présentées sous la forme d’un document HTML mis en forme par des feuilles de style CSS et rendu interactif par différentes fonctions JavaScript.

La version actuelle de l’application peut être consultée à l’adresse suivante [https://prisse.ifao.egnet.net/](https://prisse.ifao.egnet.net/).

## Installation

L’application peut être déployée sur toute installation eXist-db en uploadant le fichier [build/papyrusprisse-1.0.0.xar](https://gitlab.huma-num.fr/nsouchon/papyrus-prisse/-/blob/main/build/papyrusprisse-1.0.0.xar?ref_type=heads) depuis le Package Manager accessible sur le Dashboard.

Cette archive contient l’ensemble des fichiers composant l’application à l’exception des photographies du papyrus [téléchargeables ici](https://sharedocs.huma-num.fr/wl/?id=dwMYbCCi2okTGemgg0GUO1vwSaK5WiNn&fmode=download). Une fois l’archive .zip téléchargée et décompressée, vous pouvez ajouter ces photographies au dossier data de l’application en utilisant eXide (File > Manage > apps > pprisse > data > Upload Files > Upload Directory).

Lors de l'installation, un utilisateur et un groupe nommés **prisse** sont créés. Le mot de passe de cet utilisateur est **prisse** et peut-être modifié depuis le User Manager du Dashboard. Il est recommandé de se connecter en tant qu'utilisateur **prisse** lors de l'ajout des photographies à l'application afin que la propriété de celles-ci soit attribuée à l'utilisateur **prisse** propriétaire par défaut de l'application. 

## Crédits

### Direction du projet :

* Chloé Ragazzoli

### Direction technique :

* Serge Rosmorduc
* Nicolas Souchon

### Translittération et traduction du texte égyptien d'après :

* Bernard Mathieu

### Encodage hiéroglyphique :

* Emil Joubert

### Rédaction des notices :

* Chloé Ragazzoli
* Nicolas Souchon

### Développement informatique :

* Serge Rosmorduc
* Nicolas Souchon
* Christian Gaubert

### Traduction anglaise :

* Penelope White

### Lecture du texte :

* Thibaut Corrion

## Partenaires

* Institut français d’archéologie orientale
* Bibliothèque nationale de France
* Institut Universitaire de France
* Sorbonne Université
* UMR 8167 Orient & Méditerranée

## Hébergement web

* Huma-Num

## Bibliothèques CSS et JavaScript utilisées

* [Bootstrap Icons](https://icons.getbootstrap.com/)
* [Intro.js](https://introjs.com/)
* [jQuery](https://jquery.com/)
* [jQuery UI](https://jqueryui.com/)
* [jQuery UI Touch Punch](https://github.com/furf/jquery-ui-touch-punch)
* [OpenSeadragon](https://openseadragon.github.io/)
* [SVGOverlay](https://github.com/openseadragon/svg-overlay)
* [Swiper](https://swiperjs.com/)

## Copyright

L’application est mise à disposition dans le cadre d’une licence Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. Une copie de cette licence peut être consultée à l’adresse [http://creativecommons.org/licenses/by-nc-sa/4.0/](http://creativecommons.org/licenses/by-nc-sa/4.0/).

Les photographies, vidéos et dessins n’ayant pas été créés par le projet sont la propriété des détenteurs des droits listés ci-dessous :
* Portrait d’Émile Prisse d’Avennes par Tell, conservé à la Bibliothèque municipale de Grenoble, Mj.2618, disponible sur Pagella : [https://numotheque.grenoblealpesmetropole.fr/notice/PAG_0001134/emile-prisse-d-avennes\$](https://numotheque.grenoblealpesmetropole.fr/notice/PAG_0001134/emile-prisse-d-avennes$)
* Peinture représentant la première cour du temple de Karnak par Émile Prisse d’Avennes, conservée à la Bibliothèque nationale de France, Département des Manuscrits, NAF 20435 (2), disponible sur Gallica : [https://gallica.bnf.fr/ark:/12148/btv1b531039735/f9](https://gallica.bnf.fr/ark:/12148/btv1b531039735/f9)
* Photographie de la statue de scribe de Nikarê, conservée au Metropolitan Museum of Art, 48.67, photographie DP240451.jpg, disponible sur le site du Metropolitan Museum of Art : [https://www.metmuseum.org/art/collection/search/543900](https://www.metmuseum.org/art/collection/search/543900)
* Documentaire sur la production contemporaine de papyrus par SciDevNet, disponible sur YouTube : [https://www.youtube.com/watch?v=sO72jfUCYSg](https://www.youtube.com/watch?v=sO72jfUCYSg)
* Extrait de J.-Fr. Champollion, Grammaire égyptienne, ou Principes généraux de l'écriture sacrée égyptienne appliquée à la représentation de la langue parlée, Paris, 1836, en ligne sur Gallica : [https://gallica.bnf.fr/ark:/12148/bpt6k61025921/f76](https://gallica.bnf.fr/ark:/12148/bpt6k61025921/f76), p. 35
* Les photographies du papyrus Prisse sont la propriété de la Bibliothèque nationale de France.